import React from "react";

// redux
import { connect } from "react-redux";

// action
import { logOut } from "../../redux/actions/auth";

// React routing
import { Redirect } from "react-router-dom";

import PATH from "../../routes/PATH";

let LogoutComponent = ({ logOut, token }) => {
  const isTokenAvailable =
    token && token !== null && token.trim().length > 0 ? true : false;

  if (isTokenAvailable) {
    logOut();
  }

  if (!isTokenAvailable) {
    return <Redirect to={PATH.logout} />;
  }

  return <div></div>;
};

const mapStateToProps = ({ auth: { token } }) => ({
  token: token,
});

const mapDispatchToProps = {
  logOut: logOut,
};

LogoutComponent = connect(mapStateToProps, mapDispatchToProps)(LogoutComponent);

export default LogoutComponent;
