import React, { lazy } from "react";

import PATH from "./PATH";

// Routes
import Logout from "../pages/auth/logout";
import { Redirect } from "react-router-dom";
const Faq = lazy(() => import("../pages/faq"));
const UserBilling = lazy(() => import("../pages/dashboard/User/billing"));
const UserInvoices = lazy(() => import("../pages/dashboard/User/invoices"));
const UserPlan = lazy(() => import("../pages/dashboard/User/plan"));
const UserSettings = lazy(() => import("../pages/dashboard/User/settings"));
const Analytics = lazy(() => import("../pages/dashboard/Analytics"));
const DashBoard = lazy(() => import("../pages/dashboard/Experience"));
const Home = lazy(() => import("../pages/home"));
const Pricing = lazy(() => import("../pages/Pricing/Pricing"));
const Contact = lazy(() => import("../pages/Contact/contact"));
const Login = lazy(() => import("../pages/auth/login"));
const Register = lazy(() => import("../pages/auth/register"));
const About = lazy(() => import("../pages/about"));

export default [
  {
    path: PATH.about,
    exact: true,
    component: About,
    isPrivate: false,
  },
  {
    path: PATH.userInvoice,
    exact: true,
    component: UserInvoices,
    isPrivate: true,
  },
  {
    path: PATH.userBilling,
    exact: true,
    component: UserBilling,
    isPrivate: true,
  },
  {
    path: PATH.userPlan,
    exact: true,
    component: UserPlan,
    isPrivate: true,
  },
  {
    path: PATH.userSettings,
    exact: true,
    component: UserSettings,
    isPrivate: true,
  },
  {
    path: PATH.user,
    exact: true,
    component: () => <Redirect to={PATH.userSettings} />,
    isPrivate: false,
  },
  {
    path: PATH.dashboard,
    exact: true,
    component: () => <Redirect to={PATH.experiences} />,
    isPrivate: true,
  },
  {
    path: PATH.analytics,
    exact: true,
    component: Analytics,
    isPrivate: true,
  },
  {
    path: PATH.experiences,
    exact: true,
    component: DashBoard,
    isPrivate: true,
  },
  {
    path: PATH.logout,
    exact: true,
    component: Logout,
    isPrivate: true,
  },
  {
    path: PATH.login,
    exact: true,
    component: Login,
    isPrivate: false,
  },
  {
    path: PATH.register,
    exact: true,
    component: Register,
    isPrivate: false,
  },
  {
    path: PATH.pricing,
    exact: true,
    component: Pricing,
    isPrivate: false,
  },
  {
    path: PATH.contact,
    exact: true,
    component: Contact,
    isPrivate: false,
  },
  {
    path: PATH.main,
    exact: true,
    component: Home,
    isPrivate: false,
  },
  {
    path: PATH.faq,
    exact: true,
    component: Faq,
    isPrivate: false,
  },
  
];
