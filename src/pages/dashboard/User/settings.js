import React from "react";

import UserLayout from "./UserLayout";

import { Formik, Field } from "formik";
import InputField from "../../../components/form/inputField";
import { MDBBtn } from "mdbreact";

import classes from "./common.module.scss";

const UserSettings = () => {
  return (
    <UserLayout>
      <Formik initialValues={{}} validationSchema={{}} onSubmit={() => {}}>
        {({
          isValid,
          isSubmitting,
          handleSubmit,
          values,
          touched,
          handleChange,
          handleBlur,
        }) => (
          <form
            method="POST"
            className="w-100 mt-2 custom-input-wrapper"
            onSubmit={handleSubmit}
          >
            <div className="row ml-3 mt-4 pt-4 col-8 p-0">
              <div className="col-12 mb-3">
                <div className={classes.paragraphTitle}>Information</div>
              </div>
              <div className="col-sm-12 col-md-6">
                <Field
                  autoComplete="off"
                  className="m-0"
                  type="text"
                  name="firstName"
                  label="First name"
                  touched={touched}
                  component={InputField}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.email}
                  variant="outlined"
                />
              </div>
              <div className="col-sm-12 col-md-6">
                <Field
                  autoComplete="off"
                  className="m-0"
                  type="text"
                  name="lastName"
                  label="Last name"
                  touched={touched}
                  component={InputField}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.lastName}
                  variant="outlined"
                />
              </div>
              <div className="col-12">
                <Field
                  autoComplete="off"
                  className="m-0"
                  type="email"
                  name="email"
                  label="Email"
                  touched={touched}
                  component={InputField}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.email}
                  variant="outlined"
                />
              </div>
            </div>
            <div className="row ml-3 col-8 p-0">
              <div className="col-12 mb-2">
                <div className={`${classes.paragraphTitle} mb-1`}>Password</div>
                <p className={classes.paragraphSubTitleSettings}>
                  Choose a strong and unique password to protect you account
                </p>
              </div>
              <div className="col-sm-12 col-md-6">
                <Field
                  autoComplete="off"
                  className="m-0"
                  type="password"
                  name="password"
                  label="Current password"
                  touched={touched}
                  component={InputField}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.password}
                  variant="outlined"
                />
              </div>
              <div className="col-12 row p-0 m-0">
                <div className="col-sm-12 col-md-6">
                  <Field
                    autoComplete="off"
                    className="m-0"
                    type="password"
                    name="newPassword"
                    label="New password"
                    touched={touched}
                    component={InputField}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.newPassword}
                    variant="outlined"
                  />
                </div>
                <div className="col-sm-12 col-md-6">
                  <Field
                    autoComplete="off"
                    className="m-0"
                    type="password"
                    name="cnfPassword"
                    label="Confirm new password"
                    touched={touched}
                    component={InputField}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.cnfPassword}
                    variant="outlined"
                  />
                </div>
                <div className="col-12 mb-4 pb-4">
                  <MDBBtn className="blueBtn m-0">Save</MDBBtn>
                </div>
              </div>
            </div>
          </form>
        )}
      </Formik>
    </UserLayout>
  );
};

export default UserSettings;
