import React from "react";

// Material UI
import { MDBInput } from "mdbreact";

// Custom input field
const InputField = ({ field, form: { touched, errors }, ...props }) => {
  return (
    <>
      <MDBInput
        {...field}
        {...props}
        error={errors[field.name] && touched[field.name] ? true : false}
        background
      />
      <div className="text-danger">
        {errors[field.name] && touched[field.name] ? errors[field.name] : ""}
      </div>
    </>
  );
};

export default InputField;
