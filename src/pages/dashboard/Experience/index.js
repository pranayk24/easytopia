import React, { useState, useEffect, useRef } from "react";

import dashBoardLayout from "../../../hoc/dashBoardLayout";

import classes from "./dashboard.module.scss";

import { connect } from "react-redux";

import {
  MDBBtn,
  MDBIcon,
  MDBContainer,
  MDBModal,
  MDBModalBody,
  MDBModalHeader,
  MDBModalFooter,
} from "mdbreact";

import "@google/model-viewer";
import axios from "axios";
import { BASE_URL } from "../../../constants";
import moment from "moment";
import { toastSuccess } from "../../../utils";

import InfiniteScroll from "react-infinite-scroller";
import NewFileUpload from "./NewFileUpload";
// import DiscoverModal from "../../../components/DiscoverModal";

let DashBoard = (props) => {
  const { token } = props;
  const [uploadedImage, setUploadedImage] = useState();
  const [newUpload, setNewUpload] = useState(false);
  const [isShare, setIsShare] = useState(false);
  const [hasMore, setHasMore] = useState(false);
  const [experience, setExperience] = useState([]);
  const [isModelOpen, setIsModelOpen] = useState(false);

  const isMounted = useRef(true);

  const getExperience = (page = 0) => {
    axios
      .get(`${BASE_URL}/3ds/experiences?from=${page * 5}&size=5`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        setExperience([...experience, ...res.data.data]);
        if (res.data.total > page * 5) {
          setHasMore(true);
        } else {
          setHasMore(false);
        }
      })
      .catch((err) => {
        console.error(`err =====>`, err);
      });
  };

  useEffect(() => {
    if (isMounted.current) {
      getExperience();
      isMounted.current = false;
    }
  });

  const toggleModal = () => setIsModelOpen(!isModelOpen);

  const shareFile = (id) => {
    setIsShare(id);
  };

  // axios
  //   .get(`${BASE_URL}/3ds/experiences/search?query=LVMH`, {
  //     headers: {
  //       Authorization: `Bearer ${token}`,
  //     },
  //   })
  //   .then((res) => {
  //     console.log("response ", res);
  //   })
  //   .catch((err) => {
  //     console.error(`err =====>`, err);
  //   });

  return (
    <div className={`${classes.experienceWrapper}`}>
      <div className="col-12 mb-4 p-0 d-flex justify-content-between aligni-items-center">
        <div className="text-md-left">
          <h1 className={classes.heading}>Experiences</h1>
          <h4 className={classes.subHeading}>
            16 Experiences{" "}
            <span className={`font-weight-normal ${classes.textBlue}`}>
              Select
            </span>
          </h4>
        </div>
        <div className="text-md-right">
          <div>
            <MDBBtn
              onClick={(_) => {
                setNewUpload(true);
                console.log("====>", props, props.changExp);
              }}
              className={classes.addBtn}
            >
              Add +
            </MDBBtn>
          </div>
        </div>
      </div>
      {newUpload && (
        <div
          className={`row mb-4 p-0 mx-auto col-12 shadow-sm ${classes.expWrapper}`}
        >
          <NewFileUpload
            uploadedImage={uploadedImage}
            updateUploadedImage={(file) => setUploadedImage(file)}
            classes={classes}
            closeAddNewCard={(_) => setNewUpload(false)}
          />
        </div>
      )}
      <InfiniteScroll
        pageStart={0}
        loadMore={getExperience}
        hasMore={hasMore}
        loader={
          <div className="loader" key={0}>
            Loading ...
          </div>
        }
      >
        {experience.map(
          (item, id) =>
            item && (
              <div
                className={`row mb-4 p-0 mx-auto col-12 shadow-sm ${classes.expWrapper}`}
                key={item.id + id}
              >
                <div className={`col-sm-12 col-md-6 ${classes.leftWrapper}`}>
                  <div className="d-flex flex-wrap align-items-center">
                    <span>
                      <MDBIcon
                        className={`mr-1 ${classes.textBlue}`}
                        icon="calendar"
                      />
                      <span className={classes.iconsSubText}>
                        {moment(item.createdAt).format("DD/MM/YYYY")}
                      </span>
                    </span>

                    <span className="ml-2">
                      <i className={`mr-1 fa fa-eye ${classes.textBlue}`} />
                      <span className={classes.iconsSubText}>4,500</span>
                    </span>

                    <span className="ml-2 flex-grow-1">
                      <i
                        className={`mr-1 fa fa-weight-hanging ${classes.textBlue}`}
                      />
                      <span className={classes.iconsSubText}>50MB</span>
                    </span>
                    <span
                      className={`mr-3 ${
                        isShare === item.id + id
                          ? classes.enableShare
                          : classes.share
                      }`}
                      onClick={(_) => shareFile(item.id + id)}
                    >
                      <i className="fa fa-share"></i>
                    </span>
                    <span onClick={toggleModal}>
                      <i className="fa fa-trash-alt" />
                    </span>
                    {isModelOpen && (
                      <MDBContainer>
                        <MDBModal isOpen={isModelOpen} toggle={toggleModal}>
                          <MDBModalHeader toggle={toggleModal}>
                            Warning
                          </MDBModalHeader>
                          <MDBModalBody color="secondary">
                            {"You want to delete this experience?"}
                          </MDBModalBody>
                          <MDBModalFooter>
                            <MDBBtn color="secondary" onClick={toggleModal}>
                              Yes
                            </MDBBtn>
                            <MDBBtn onClick={toggleModal} color="primary">
                              No
                            </MDBBtn>
                          </MDBModalFooter>
                        </MDBModal>
                      </MDBContainer>
                    )}
                  </div>
                  <div>
                    <h4 className={classes.heading}>{item.scene.name}</h4>
                  </div>
                  <div>
                    {item.tags.map((item) => (
                      <span className={classes.tag} key={item.id}>
                        {item.name}
                      </span>
                    ))}
                  </div>
                  <div>
                    <p className={classes.content}>{item.description}</p>
                  </div>
                  <div className="custom-control custom-switch">
                    <input
                      type="checkbox"
                      className="custom-control-input"
                      id={`rc-${item.id}`}
                      checked={id % 2 === 0}
                      onChange={(e) => {
                        console.log(`e =====>`, e);
                      }}
                    />
                    <label
                      className="custom-control-label"
                      htmlFor="customSwitches"
                    >
                      Active
                    </label>
                  </div>
                </div>
                <div className={`col-sm-12 col-md-6 p-0 ${classes.rightPanel}`}>
                  {isShare !== item.id + id ? (
                    <>
                      <model-viewer
                        ar
                        auto-rotate
                        camera-controls
                        autoplay
                        style={{ width: "100%", height: "100%" }}
                        alt="Experience name"
                        ar-modes="webxr scene-viewer quick-look fallback"
                        src={`https://d3vkzotno2n070.cloudfront.net/${item.pictures.main}`}
                        ios-src={`https://d3vkzotno2n070.cloudfront.net/${item.pictures.main}`}
                      ></model-viewer>
                    </>
                  ) : (
                    <div className={classes.sharingWrapper}>
                      <p>Share</p>
                      <div className="position-relative">
                        <input
                          id={`r-${item.id}`}
                          value={item.id}
                          disabled
                          className={classes.input}
                        ></input>
                        <MDBBtn
                          onClick={(_) => {
                            navigator.clipboard.writeText(item.id);
                            toastSuccess(`${item.id} copied successfully.`);
                            setIsShare(false);
                          }}
                          className={classes.copy}
                        >
                          Copy
                        </MDBBtn>
                      </div>
                    </div>
                  )}
                </div>
              </div>
            )
        )}
      </InfiniteScroll>
    </div>
  );
};

const mapStateToProps = ({ auth: { token, loading, changExp } }) => ({
  token: token,
  loading: loading,
  changExp: changExp,
});

export default connect(mapStateToProps, null)(dashBoardLayout(DashBoard));
