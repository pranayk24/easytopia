import React from "react";

import UserLayout from "./UserLayout";

import classes from "./common.module.scss";
import { MDBBtn } from "mdbreact";

const UserPlan = () => {
  return (
    <UserLayout>
      <div className="row ml-3 mt-4 pt-4 col-8 p-0">
        <div className="col-12 mb-0">
          <div className={`${classes.paragraphTitle} mb-1`}>Your plan</div>
          <p className={classes.paragraphSubTitlePlan}>
            Learn more about our plan
          </p>
        </div>
        <div className="col-5 mb-3">
          <select defaultValue="1" className="custom-select">
            <option value="1">Business</option>
            <option value="2">Two</option>
            <option value="3">Three</option>
          </select>
        </div>
        <div className="col-12 mb-4 pb-4">
          <MDBBtn className="blueBtn px-3 py-2 m-0">Change Plan</MDBBtn>
        </div>
      </div>
    </UserLayout>
  );
};

export default UserPlan;
