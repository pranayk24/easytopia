import { ME_GET_FAIL, ME_GET_SUCCESS, ME_CLEAR } from "../constants/root";

let userInitData = localStorage.getItem("user");
userInitData = JSON.parse(userInitData) ? JSON.parse(userInitData) : null;

const initState = {
  user: userInitData,
  error: null,
  rootLoading: false,
};

const rootReducer = (state = initState, { type, payload }) => {
  switch (type) {
    case ME_GET_FAIL:
      return {
        ...state,
        error: payload,
      };

    case ME_GET_SUCCESS:
      return {
        ...state,
        error: false,
        user: payload,
      };

    case ME_CLEAR:
      return {
        user: null,
        error: null,
        rootLoading: false,
      };

    default:
      return state;
  }
};

export default rootReducer;
