import React, { useEffect } from "react";

import { MDBBtn } from "mdbreact";

import classes from "./login.module.scss";

// Amazon cognito
import UserPool from "../../aws/UserPool";
import { CognitoUserAttribute } from "amazon-cognito-identity-js";

// redux
import { connect } from "react-redux";
import * as actions from "../../redux/actions/auth";

//
import { Formik, Field } from "formik";
import * as Yup from "yup";
import InputField from "../../components/form/inputField";
import { Redirect, NavLink } from "react-router-dom";
import { toastError } from "../../utils";

import PATH from "../../routes/PATH";

const registerFormValidation = Yup.object().shape({
  email: Yup.string()
    .email("Invalid email!")
    .required("The Email field is required!"),
  password: Yup.string().required("The Password field is required!"),
});

const Register = ({ auth, start, end, cleanUp, history }) => {
  const { token, loading } = auth;

  useEffect(() => {
    return () => {
      cleanUp();
    };
  }, [cleanUp]);

  if (token && typeof token === "string" && token.trim().length > 0) {
    return <Redirect to={PATH.main} />;
  }

  return (
    <div className={`row custom-input-wrapper ${classes.loginWrapper}`}>
      <Formik
        initialValues={{
          email: "",
          password: "",
          alias: "",
        }}
        validationSchema={registerFormValidation}
        onSubmit={async ({ alias, email, password }, { setSubmitting }) => {
          start();
          var attributeList = [];

          attributeList.push(
            new CognitoUserAttribute({
              Name: "email",
              Value: email,
            })
          );

          console.log(`UserPool =====>`, UserPool);

          UserPool.signUp(alias, password, attributeList, null, function (
            err,
            result
          ) {
            if (err) {
              if (err.code === "UsernameExistsException") {
                toastError("User is already register, Please login.");
              } else {
                toastError(
                  err.message
                    ? err.message
                    : "Something went wrong, Please try again."
                );
              }
            }
            setSubmitting(false);
            end();
            if (result && result.user) {
              var cognitoUser = result.user;
              console.log(`result =====>`, result);
              console.log(`cognitoUser =====>`, cognitoUser);
              history.push(PATH.login);
            }
          });
        }}
      >
        {({
          isValid,
          isSubmitting,
          handleSubmit,
          values,
          touched,
          handleChange,
          handleBlur,
        }) => (
          <form method="POST" className="w-100" onSubmit={handleSubmit}>
            <div className="col-sm-12 col-lg-4">
              <h1 className={`mt-5 ${classes.headerText}`}>Register</h1>
              <h5 className={classes.subHeaderText}>Welcome aboard!</h5>

              <Field
                autoComplete="off"
                className="m-0 mt-4"
                type="email"
                name="email"
                label="Email"
                touched={touched}
                component={InputField}
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.email}
                variant="outlined"
              />

              <Field
                autoComplete="off"
                className="m-0"
                type="text"
                name="alias"
                label="Username"
                touched={touched}
                component={InputField}
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.alias}
                variant="outlined"
              />

              <Field
                className="m-0"
                type="password"
                name="password"
                label="Password"
                touched={touched}
                component={InputField}
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.password}
                variant="outlined"
              />

              <div className="row">
                <div className="col-sm-12 col-md-6 d-md-flex align-items-center"></div>
                <div className="col-sm-12 text-lg-right">
                  <MDBBtn
                    disabled={isSubmitting || !isValid || loading}
                    className={`blueBtn w-100 m-0 mb-4`}
                    type="submit"
                  >
                    Register
                  </MDBBtn>

                  <div className={`${classes.forgotText} mt-2`}>
                    Already have account?{" "}
                    <NavLink
                      className={`${classes.textBlue}  text-decoration-none`}
                      to={PATH.login}
                    >
                      Login
                    </NavLink>
                  </div>
                </div>
              </div>
            </div>
          </form>
        )}
      </Formik>
    </div>
  );
};

const mapStateToProps = ({ auth }) => ({
  auth: auth,
});

const mapDispatchToProps = {
  start: actions.startLoader,
  end: actions.endLoader,
  logIn: actions.logIn,
  cleanUp: actions.clean,
};

export default connect(mapStateToProps, mapDispatchToProps)(Register);
