import React, { useState, useEffect, useRef } from "react";
import { NavLink } from "react-router-dom";
import axios from "axios";
import { BASE_URL } from "../../constants";
import { formatBytes } from "../../utils/index";
import classes from "./price.module.scss";
import PATH from "../../routes/PATH";
import { MDBBtn } from "mdbreact";
import { useStripe, useElements } from "@stripe/react-stripe-js";
// redux
import { connect } from "react-redux";

// localization
import { FormattedMessage } from "react-intl";
const Pricing = ({ token }) => {
  const stripe = useStripe();
  const elements = useElements();
  console.log("abc", elements);
  const [data, setData] = useState([]);
  const isMounted = useRef(true);
  //  check for token to display register button
  const isTokenAvailable =
    token && token !== null && token.trim().length > 0 ? true : false;

  useEffect(() => {
    if (isMounted.current) {
      getPlans();
      isMounted.current = false;
    }
  }, [data]);

  async function getPlans() {
    axios
      .get(`${BASE_URL}/plans`)
      .then((response) => {
        console.log("Response", response.data);
        setData(response.data.data);
      })
      .catch((err) => {
        console.log("Error", err);
      });

    // axios
    //   .get(`https://api.stripe.com/v1/products/prod_HHIpNMoudN3Ovt`, {
    //     headers: {
    //       Authorization: `Bearer ${
    //         (process.env.REACT_APP_STRIPE_PK,
    //         "sk_test_ZrE2LFjm2ThTjPgQqzN7lnLb00ZhPmNY7w")
    //       }`,
    //     },
    //   })
    //   .then((resp) => {
    //     console.log("resp", resp);
    //   })
    //   .catch((err) => {
    //     console.log("err", err);
    //     // toastError("Something went wrong! Please try again after sometime,");
    //   });
  }
  return (
    <div className={classes.homeWrapper}>
      <div className="row">
        <div className={`${classes.col}`}>
          <h3 className={`font-weight-bold m-0 ${classes.heading}`}>
            <FormattedMessage
              id="pages.home.pricing.Pricing"
              defaultMessage="Pricing"
            />
          </h3>
          <h5 className={`font-weight-normal m-0 ${classes.subheading}`}>
            <FormattedMessage
              id="pages.home.pricing.Therightprice"
              defaultMessage="The right price for you"
            />
          </h5>
          {!isTokenAvailable && (
            <h5 className={`font-weight-normal m-0 ${classes.secSubheading}`}>
              <FormattedMessage
                id="pages.home.pricing.CreateFreeAcc"
                defaultMessage="Create a free account to discover our web analytics dashboard for
              your AR experiences"
              />
            </h5>
          )}
          {!isTokenAvailable && (
            // <NavLink to={PATH.register} className={`${classes.contact}`}>
            //   Register
            // </NavLink>
            <a href={PATH.register}>
              <MDBBtn className="blueBtn w-30 m-0">
                <FormattedMessage
                  id="pages.home.pricing.Register"
                  defaultMessage="Register"
                />
              </MDBBtn>
            </a>
          )}
        </div>
      </div>
      <div className="row">
        {data.map((item) => (
          <div className={`${classes.card}`} key={item.id}>
            <h5 className={`${classes.basic}`}>{item.nickname}</h5>
            <p style={{ marginLeft: "25px", fontWeight: "400" }}>
              {item.metadata.description}
            </p>
            <h6
              style={{
                marginLeft: "25px",
                marginBottom: "30px",
                fontWeight: 400,
                fontSize: "20px",
                height: "45px",
              }}
            >
              {item.unit_amount == "0" && item.metadata.custom_price == "1" ? (
                <a href={PATH.contact}>
                  <MDBBtn className="blueBtn w-30 m-0">
                    <FormattedMessage
                      id="pages.home.pricing.contact"
                      defaultMessage="Contact"
                    />
                  </MDBBtn>
                </a>
              ) : item.unit_amount == "0" &&
                item.metadata.custom_price == "0" ? (
                "Free"
              ) : item.unit_amount != "0" ? (
                `${item.unit_amount / 100} €/mo`
              ) : null}
            </h6>
            {/* {item.yearlyPrice && (
              <h6 style={{ marginLeft: "25px" }}>{item.yearlyPrice}€/yr</h6>
            )} */}
            <p className={`${classes.line}`} />
            {/* {item.metadata.time_limit === "0" ||
            item.metadata.time_limit === null ? null : (
              <p>
                {" "}
                <i className={`${classes.check}`} />
                Link valid for {item.metadata.time_limit} days
              </p>
            )}
        

            <p>
              {" "}
              <i className={`${classes.check}`} />
              Upload upto {formatBytes(item.storageLimit)}
            </p>
            
              {" "}
              <i className={`${classes.check}`} />
              Up to {item.viewsLimit} views */}
            {item.metadata.features_1 == undefined ? null : (
              <p style={{ display: "flex", flexDirection: "row" }}>
                {" "}
                <i className={`${classes.check}`} />
                <div style={{ paddingLeft: "10" }}>
                  {item.metadata.features_1}
                </div>
              </p>
            )}
            {item.metadata.features_2 == undefined ? null : (
              <p style={{ display: "flex", flexDirection: "row" }}>
                {" "}
                <i className={`${classes.check}`} />
                <div style={{ paddingLeft: "10" }}>
                  {item.metadata.features_2}
                </div>
              </p>
            )}
            {item.metadata.features_3 == undefined ? null : (
              <p style={{ display: "flex", flexDirection: "row" }}>
                {" "}
                <i className={`${classes.check}`} />
                <div style={{ paddingLeft: "10" }}>
                  {item.metadata.features_3}
                </div>
              </p>
            )}
            {console.log("why its not null", item.metadata.features_4)}
            {item.metadata.features_4 == undefined ? null : (
              <p style={{ display: "flex", flexDirection: "row" }}>
                {" "}
                <i className={`${classes.check}`} />
                <div style={{ paddingLeft: "10" }}>
                  {item.metadata.features_4}
                </div>
              </p>
            )}
            {item.metadata.features_5 == undefined ? null : (
              <p style={{ display: "flex", flexDirection: "row" }}>
                {" "}
                <i className={`${classes.check}`} />
                <div style={{ paddingLeft: "10" }}>
                  {item.metadata.features_5}
                </div>
              </p>
            )}
            {item.metadata.features_6 == undefined ? null : (
              <p style={{ display: "flex", flexDirection: "row" }}>
                {" "}
                <i className={`${classes.check}`} />
                <div style={{ paddingLeft: "10" }}>
                  {item.metadata.features_6}
                </div>
              </p>
            )}
            {item.metadata.features_7 == undefined ? null : (
              <p style={{ display: "flex", flexDirection: "row" }}>
                {" "}
                <i className={`${classes.check}`} />
                <div style={{ paddingLeft: "10" }}>
                  {item.metadata.features_7}
                </div>
              </p>
            )}
          </div>
        ))}
      </div>
      <div className={`row ${classes.options}`}>
        <div className={`col-xs-12 col-sm-12 col-md-6 ${classes.option1}`}>
          <div className={`${classes.note}`}>
            <h4 style={{ fontWeight: "500" }}>
              <FormattedMessage
                id="pages.home.pricing.Needahelp"
                defaultMessage="Need a help to choose your plan?"
              />
            </h4>
            <p>
              <FormattedMessage
                id="pages.home.pricing.Wearehere"
                defaultMessage="We are here to help to find the right plan based on your needs."
              />
            </p>
            <NavLink to={PATH.contact} className={`${classes.contact}`}>
              <FormattedMessage
                id="pages.home.pricing.contact"
                defaultMessage="Contact us"
              />
            </NavLink>
          </div>
        </div>
        <div className={`col-xs-12 col-sm-12 col-md-6 ${classes.option2}`}>
          <div className={`${classes.note}`}>
            <h4 style={{ fontWeight: "500" }}>
              <FormattedMessage
                id="pages.home.pricing.CustomizePlan"
                defaultMessage="Customize plan"
              />
            </h4>
            <p>
              <FormattedMessage
                id="pages.home.pricing.Lookingforsomething"
                defaultMessage=" Looking for something else? Request a consultation to customize aplan."
              />
            </p>
            <NavLink to={PATH.contact} className={`${classes.contact}`}>
              <FormattedMessage
                id="pages.home.pricing.contact"
                defaultMessage="Contact us"
              />
            </NavLink>
          </div>
        </div>
      </div>
    </div>
  );
};

const mapStateToProps = ({ auth: { token } }) => ({
  token: token,
});
// export default Pricing;
export default connect(mapStateToProps)(Pricing);
