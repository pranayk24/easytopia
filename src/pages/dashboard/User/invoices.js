import React from "react";

import UserLayout from './UserLayout'

const UserInvoices = () => {
  return (
    <UserLayout>
      <div>
        Invoices
      </div>
    </UserLayout>
  );
};

export default UserInvoices;
