import React, { useEffect, useState } from "react";

import classes from "./dashboard_layout.module.scss";

import { NavLink } from "react-router-dom";

// images
import Logo from "../assets/images/logo.png";
import { MDBIcon } from "mdbreact";

import PATH from "../routes/PATH";

// localization
import { FormattedMessage } from "react-intl";

import FormLabel from "@material-ui/core/FormLabel";
import FormControl from "@material-ui/core/FormControl";
import FormGroup from "@material-ui/core/FormGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormHelperText from "@material-ui/core/FormHelperText";
import Checkbox from "@material-ui/core/Checkbox";

import { withStyles } from "@material-ui/core/styles";
import { cyan } from "@material-ui/core/colors";

import { MDBBtn } from "mdbreact";
import { TextField } from "@material-ui/core";
import { setNestedObjectValues } from "formik";

import axios from "axios";
import { BASE_URL2 } from "../constants";

import { changExperience } from "../redux/actions/auth";
import { connect } from "react-redux";

const GreenCheckbox = withStyles({
  root: {
    color: "#837e7e",
    "&$checked": {
      color: cyan[500],
    },
  },
  checked: {},
})((props) => <Checkbox color="default" {...props} />);

var changeExp = null;

const Design = (props) => {
  const { children, navigation } = props;
  console.log("akash", props.children.props.token);
  var token = props.children.props.token;
  console.log("token", token);
  const [data, setData] = useState([]);
  const [isClicked, setClick] = useState(false);

  const [checkBox, setCheckBox] = useState([
    {
      id: "0",
      name: "LVMH",
      label: "LVMH",
      status: false,
    },
    {
      id: "1",
      name: "DIOR",
      label: "DIOR",
      status: false,
    },
    {
      id: "2",
      name: "unlisted",
      label: "Unlisted",
      status: false,
    },
  ]);

  // setCheckBox();
  // const [Fields, setFields] = useState([]);
  // const [state] = React.useState({
  //   LVMH: false,
  //   DIOR: false,
  //   Unlisted: false,
  // });

  const handleChange = (event, index) => {
    let check = [...checkBox];
    check[index].status = !check[index].status;
    console.log("==>", check[index].label);
    props.children.props.changExperience(check[index].label);
    // props.navigation.setParams({ title: t });
    changeExp = check[index].label;

    setCheckBox(check);
  };

  const add2List = (event) => {
    console.log("event =>", event.target.value);

    let formDataExp = new FormData();
    formDataExp.append("name", event.target.value);
    formDataExp.append("experiences", ["3cab5329-4380-4f74-ae0e-ec162266bb36"]);
    axios
      .post(`${BASE_URL2}/campaigns`, formDataExp, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        console.log("==>", res);
      })
      .catch((err) => {
        console.error(`err =====>`, err);
      });

    let check1 = [...checkBox];
    console.log(check1.length);
    let obj = {
      id: check1.length,
      name: event.target.value,
      label: event.target.value,
      status: false,
    };
    check1.push(obj);
    console.log(check1);
    setCheckBox(check1);
    setClick(false);
  };

  useEffect(() => {
    document.getElementsByTagName("body")[0].classList.remove("body-style-one");
    document.getElementById("et-footer").classList.add("d-none");
    // document.getElementsByClassName("et-container")[0].style[
    //   "padding-bottom"
    // ] = 0;

    return () => {
      document.getElementsByTagName("body")[0].classList.add("body-style-one");
      // document.getElementsByClassName("et-container")[0].style[
      //   "padding-bottom"
      // ] = "200px";
      document.getElementById("et-footer").classList.remove("d-none");
    };
  });

  const createCampaign = (e) => {
    var values = [];
    setData = values;
  };

  const openFields = () => {
    // var clicked = true;
    setClick(true);
  };
  // Api integration

  // axios
  //   .get(`${BASE_URL2}/campaigns`, {
  //     headers: {
  //       Authorization: `Bearer ${token}`,
  //     },
  //   })
  //   .then((res) => {
  //     console.log("==>", res);
  //   })
  //   .catch((err) => {
  //     console.error(`err =====>`, err);
  //   });

  return (
    <div className={`${classes["dashboard-main-wrapper"]}`}>
      <nav className={classes.sidebar}>
        <ul>
          <li className={classes.sidebarLogo}>
            <NavLink to={PATH.main} className="d-flex align-items-center">
              <img
                className="d-inline-block align-top"
                width="34"
                height="34"
                src={Logo}
                alt=""
              />
              <b className={`${classes.sidebarLogoText} font-weight-bold ml-1`}>
                EASYTOPIA
              </b>
            </NavLink>
          </li>
          <li className={classes.userWrapper}>
            <NavLink className="text-decoration-none" to={PATH.userSettings}>
              <div className="d-flex align-items-center">
                <div className="d-flex flex-grow-1">
                  <div className={`${classes.avatar} mr-1`}>GF</div>
                  <div>
                    <div className={classes.name}>Gauthier F.</div>
                    <div className={classes.tag}>Business</div>
                  </div>
                </div>

                <div>
                  <MDBIcon
                    className="text-decoration-none text-muted"
                    icon="angle-right"
                  />
                </div>
              </div>
            </NavLink>
          </li>
        </ul>
        <div>
          <div className={classes.Campaign}>Campaign </div>

          {checkBox.map((i, index) => (
            <FormControlLabel
              key={index}
              control={
                <GreenCheckbox
                  // checked={state.checkedC}
                  checked={i.status}
                  onChange={(e) => {
                    handleChange(e, index);
                  }}
                  name={i.label}
                  color="primary"
                />
              }
              label={i.label}
              className={classes.checkBox}
            />
          ))}
          {isClicked && (
            <input className={classes.inputCam} onBlur={add2List} />
          )}
          <MDBBtn
            className="blueBtn w-100 m-0"
            id={classes.blueBtn}
            // onClick={createCampaign}
            onClick={openFields}
          >
            {/* <FormattedMessage
              id="pages.home.pricing.Register"
              defaultMessage="New Campaign"
            /> */}
            New campaign &nbsp; &nbsp; &nbsp; +
          </MDBBtn>
        </div>
      </nav>

      <div className={classes.contentWrapper}>
        <header
          className={`d-flex align-items-center ${classes.dashboardHeader}`}
        >
          <NavLink
            activeClassName={classes.active}
            className={classes.linkStyle}
            to={PATH.experiences}
          >
            <FormattedMessage
              id="hoc.Experiences"
              defaultMessage="Experiences"
              changeExperience="prk"
            />
          </NavLink>
          <NavLink
            activeClassName={classes.active}
            className={classes.linkStyle}
            to={PATH.analytics}
          >
            <FormattedMessage id="hoc.Analytics" defaultMessage="Analytics" />
          </NavLink>
        </header>
        <div className={classes.content}>{children}</div>
      </div>
    </div>
  );
};

export default (WrappedComponent) => {
  const hocComponent = ({ ...props }) => (
    <Design>
      <WrappedComponent {...props} />
    </Design>
  );

  const mapStateToProps = ({ auth: { token, language, flag } }) => ({
    token: token,
    language: language,
    flag: flag,
  });

  const mapDispatchToProps = {
    changExperience,
  };

  const Designx = connect(mapStateToProps, mapDispatchToProps)(hocComponent);

  return Designx;
};
