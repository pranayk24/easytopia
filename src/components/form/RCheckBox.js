import React from "react";

const RCheckBox = ({ label, onChange, id, ...rest}) => {
  return (
    <div className="mr-2 custom-control custom-switch">
      <input
        type="checkbox"
        className="custom-control-input cursor-pointer"
        onClick={(e) => onChange(e.target.checked)}
        id={id}
        { ...rest }
      />
      <label className="custom-control-label cursor-pointer" htmlFor={id}>
        {label}
      </label>
    </div>
  );
};

export default RCheckBox;
