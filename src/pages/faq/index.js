import React, { useState } from "react";
import { NavLink } from "react-router-dom";
import classes from "./faq.module.scss";
import PATH from "../../routes/PATH";
const paragraph =
  "Lorem ipsum dolor sit amet consectetur adipisicing elit. Eveniet natus sint provident vel ab reprehenderit cum soluta, suscipit facere nisi sed earum repellendus fuga debitis, nam molestiae minima voluptates possimus.";

const data = [
  {
    title: "Pricing plans",
    paragraph,
  },
  {
    title: "How to apply",
    paragraph,
  },
  {
    title: "Purchasing process",
    paragraph,
  },
  {
    title: "Usage guides",
    paragraph,
  },
];

export const AccordionItem = ({ paragraph, title }) => {
  const [isOpen, setIsOpen] = useState(false);
  console.log(`title, isOpen =====>`, title, isOpen);
  return (
    <div
      className={`${classes["accordion-item"]} ${
        isOpen && classes["accordion-item--opened"]
        }`}
      onClick={() => {
        setIsOpen(!isOpen);
      }}
    >
      <div className={classes["accordion-item__line"]}>
        <h3 className={classes["accordion-item__title"]}>{title}</h3>
        <span className={classes["accordion-item__icon"]} />
      </div>
      <div className={classes["accordion-item__inner"]}>
        <div className={classes["accordion-item__content"]}>
          <p className={classes["accordion-item__paragraph"]}>{paragraph}</p>
        </div>
      </div>
    </div>
  );
};

const Faq = () => {
  return (
    <div className={classes.wrapper}>
      <h1 className={classes.title}>FAQ</h1>
      <ul className={classes["accordion-list"]}>
        {data.map((data, key) => {
          return (
            <li key={key} className={classes["accordion-list__item"]}>
              <AccordionItem {...data} />
            </li>
          );
        })}
      </ul>
      <div className="mt-3 text-center">
        <p className='mb-1'> I do not find my question</p>
        <NavLink to={PATH.contact} >
          Contact us
            </NavLink>
      </div>
    </div>
  );
};

export default Faq;
