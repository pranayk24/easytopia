export const toastConfig = {
  position: "top-right",
  autoClose: 5000,
  hideProgressBar: true,
  closeOnClick: true,
  pauseOnHover: true,
  draggable: true,
  progress: undefined,
};

export const BASE_URL = "https://dev.easytopia.app/api";
export const BASE_URL2 = "https://dev.easytopia.app";
// export const BASE_URL = "https://ba4158c00717.ngrok.io/api";
