import React, { useState } from "react";

//
import { Formik, Field } from "formik";
import * as Yup from "yup";
import InputField from "../../../components/form/inputField";

import RCheckBox from "../../../components/form/RCheckBox";

import StyledDropzone from "../../../components/StyledDropzone";
import { MDBBtn } from "mdbreact";

import Select from "react-select";

// localization
import { FormattedMessage } from "react-intl";

const validation = Yup.object().shape({
  email: Yup.string()
    .trim()
    .email("Invalid email!")
    .required("The Email field is required!"),
  alias: Yup.string().required("The Alias field is required!"),
  productLink: Yup.string()
    .trim()
    .matches(
      /(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&=]*)/,
      "URL format is not valid"
    ),
});

const NewFileUpload = ({
  closeAddNewCard,
  classes,
  uploadedImage,
  updateUploadedImage,
}) => {
  const [isPrivate, setIsPrivate] = useState(false);
  const [previewImg, setPreviewImg] = useState("");

  return (
    <Formik
      initialValues={{
        title: "",
        productLink: "",
        description: "",
      }}
      validationSchema={validation}
      onSubmit={(_) => {}}
    >
      {({
        isValid,
        isSubmitting,
        handleSubmit,
        values,
        touched,
        handleChange,
        handleBlur,
      }) => (
        <form method="POST" className="w-100 mt-2" onSubmit={handleSubmit}>
          <div className="p-3 w-100">
            <div className="row">
              <div className="col-12 d-flex align-items-center mb-2">
                <span className="mr-2 font-weight-bold">New experience</span>

                <div className="mr-2">
                  <RCheckBox
                    label="Active"
                    id="new-active"
                    defaultChecked
                    onChange={(value) => {
                      console.log(`value =====>`, value);
                    }}
                  />
                </div>
                <div className="mr-2">
                  <RCheckBox
                    label="Private"
                    id="new-private"
                    defaultValue={isPrivate}
                    onChange={(value) => {
                      setIsPrivate(value);
                    }}
                  />
                </div>
              </div>
              <div className="col-4">
                <div className={`ml-2 mb-3 ${classes.rowTitle}`}>
                  File upload
                </div>
                <StyledDropzone
                  uploadedImage={uploadedImage}
                  updateUploadedImage={updateUploadedImage}
                />
                <input
                  style={{ display: "none" }}
                  id="etPreviewImg"
                  name="etPreviewImg"
                  type="file"
                  accept="image/*"
                  onChange={(e) => setPreviewImg(e.target.files[0])}
                />
                {typeof previewImg !== "object" && (
                  <label
                    htmlFor="etPreviewImg"
                    className="text-center py-2 w-100 m-0 mt-3 outlinedBlueBtn"
                  >
                    Add preview image
                  </label>
                )}
                {typeof previewImg === "object" && (
                  <div className="d-flex flex-wrap align-items-center justify-content-between mt-3">
                    <img
                      width={42}
                      height={42}
                      style={{
                        border: "1.5px solid #1bc9e6",
                        borderRadius: 4,
                      }}
                      src={URL.createObjectURL(previewImg)}
                      alt=""
                    />
                    <label
                      htmlFor="etPreviewImg"
                      className="text-center py-2 px-4 m-0 outlinedBlueBtn"
                    >
                      Change preview image
                    </label>
                  </div>
                )}
              </div>
              <div className="col-4 custom-input-wrapper">
                <div className={`mb-3 ${classes.rowTitle}`}>Info</div>
                <Field
                  autoComplete="off"
                  className="m-0"
                  type="text"
                  name="title"
                  label="Title"
                  touched={touched}
                  component={InputField}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.title}
                  variant="outlined"
                />

                <Field
                  autoComplete="off"
                  className="m-0"
                  type="text"
                  name="productLink"
                  label={
                    <FormattedMessage
                      id="component.Dashboard.Experince.ProductLink"
                      defaultMessage="Product Link"
                    />
                  }
                  touched={touched}
                  component={InputField}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.productLink}
                  variant="outlined"
                />

                <Field
                  autoComplete="off"
                  className="m-0"
                  type="textarea"
                  name="description"
                  label="Description"
                  touched={touched}
                  component={InputField}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.description}
                  variant="outlined"
                />

                <Select
                  isMulti
                  name="tags"
                  placeholder="Tags..."
                  className="basic-multi-select"
                  classNamePrefix="select"
                  styles={{
                    control: (styles) => ({
                      ...styles,
                      backgroundColor: "#f5f5f5",
                      border: "none",
                    }),
                  }}
                  options={[
                    { value: "autres", label: "Autres" },
                    { value: "test", label: "Test" },
                  ]}
                />
              </div>
              {!isPrivate && (
                <div className="p-0 pl-3 row col-4">
                  <div className="p-0 px-1 col-sm-12 col-md-8">
                    <p className={classes.rowTitle}>Advanced settings</p>
                    <RCheckBox
                      label="Default Lighting"
                      id="AddNew-DefaultLighting"
                      onChange={(value) => {
                        console.log(`value =====>`, value);
                      }}
                    />

                    <RCheckBox
                      label="Auto update lighting"
                      id="AddNew-AutoUpdateLighting"
                      onChange={(value) => {
                        console.log(`value =====>`, value);
                      }}
                    />

                    <RCheckBox
                      label="PBR"
                      id="AddNew-PBR"
                      onChange={(value) => {
                        console.log(`value =====>`, value);
                      }}
                    />

                    <RCheckBox
                      label="Shadows"
                      id="AddNew-Shadows"
                      onChange={(value) => {
                        console.log(`value =====>`, value);
                      }}
                    />

                    <RCheckBox
                      label="People occlusion"
                      id="AddNew-PeopleOcclusion"
                      onChange={(value) => {
                        console.log(`value =====>`, value);
                      }}
                    />
                  </div>
                  <div className="p-0 px-1 col-sm-12 col-md-4">
                    <p className={classes.rowTitle}>Application</p>

                    <RCheckBox
                      label="iOS"
                      id="AddNew-iOS"
                      onChange={(value) => {
                        console.log(`value =====>`, value);
                      }}
                    />

                    <RCheckBox
                      label="Android"
                      id="AddNew-Android"
                      onChange={(value) => {
                        console.log(`value =====>`, value);
                      }}
                    />
                  </div>
                </div>
              )}
              <div className="col-12 text-right">
                <MDBBtn onClick={closeAddNewCard} outline className="textBtn">
                  Cancel
                </MDBBtn>
                <MDBBtn className="blueBtn px-5">Save</MDBBtn>
              </div>
            </div>
          </div>
        </form>
      )}
    </Formik>
  );
};

export default NewFileUpload;
