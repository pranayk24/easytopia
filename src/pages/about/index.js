import React from "react";

import classes from "./index.module.scss";

import RyanImage from "../../assets/images/ryan.png";
import BenoitImage from "../../assets/images/benoit.png";
import GauthierImage from "../../assets/images/gauthier.png";

import {
  MDBCard,
  MDBCardBody,
  MDBCardImage,
  MDBCardTitle,
  MDBRow,
  MDBCol,
  MDBView,
} from "mdbreact";

const TeamObj = [
  {
    image: GauthierImage,
    name: "Gauthier Ferret",
    pos: "CEO",
  },
  {
    image: RyanImage,
    name: "Ryan Giunchi",
    pos: "CTO",
  },
  {
    image: BenoitImage,
    name: "Benoit Arnaudo",
    pos: "3D expert",
  },
];

const TeamCard = ({ image, name, pos }) => (
  <MDBCol md="4">
    <MDBCard wide cascade>
      <MDBView cascade>
        <MDBCardImage
          hover
          overlay="white-slight"
          className="card-img-top"
          src={image}
          alt={name}
        />
      </MDBView>

      <MDBCardBody cascade className="text-center p-2">
        <MDBCardTitle className="card-title mb-0">
          <strong className="h5">{name}</strong>
        </MDBCardTitle>

        <p className="font-weight-bold blue-text mb-0 h5">{pos}</p>
      </MDBCardBody>
    </MDBCard>
  </MDBCol>
);

const About = () => {
  let renderTeam = TeamObj.map((item) => <TeamCard {...item} />);

  return (
    <div className={classes.aboutWrapper}>
      <h1 className={classes.title}>About</h1>
      <p className={`mt-1 ${classes.para}`}>Meet Easytopia</p>
      <p className={classes.para2}>
        The leading innovation studio which only focuses on Augmented Reality.
      </p>
      <p className={classes.para2}>2 years of R&D.</p>
      <p className={classes.para2}>The best photorealistic rendering.</p>
      <p className={classes.para2}>Artificial Intelligence.</p>
      <p className={`mt-3 ${classes.para2}`}>
        Buyers do not buy products, they buy experiences…
      </p>
      <p className={`mt-4 font-weight-bold ${classes.secTitle}`}>Team</p>
      <MDBRow className="mx-auto my-4">{renderTeam}</MDBRow>

      {/* <p className={`mt-4 font-weight-bold ${classes.secTitle}`}>
        Testimonials
      </p> */}

      {/* <MDBRow className="my-4">
        <MDBCol md="6">
          <MDBCard>
            <div className="d-flex p-2">
              <img
                style={{
                  borderRadius: "50%",
                  width: "100%",
                  width: 120,
                  height: 120,
                  alignSelf: "center",
                }}
                src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxAQEBAQEBANEBAVEBINDQ0VDRsQEA4NIB0iIiAdHx8kKDQsJCYxJx8fLTItMT1AMEMwIys/QD8uTDRBMDcBCgoKDQ0OFQ8QFTcZFRkrKy03KzcxNzc3Kys3LSs3Nzc3Ky0rKys3LSsrLTg3My0rKysrKzctLSstKy0rKysrK//AABEIAMgAyAMBIgACEQEDEQH/xAAcAAABBQEBAQAAAAAAAAAAAAAEAAIDBQYBBwj/xAA/EAABAwIDBQQHBgUDBQAAAAABAAIDBBEFEiExQVFhcQYTIoEHIzJyobHBM0JSYpHwFCRzgtEVU5IlQ3Th8f/EABoBAAMBAQEBAAAAAAAAAAAAAAECAwQABQb/xAAnEQACAgIBBAIBBQEAAAAAAAAAAQIRAzEhBBIyQSJCURMjM2FxFP/aAAwDAQACEQMRAD8A1+DNtmH57rQgKjw0au94K+C8zFo1ZNiARMO0qABEwjU9FZbJS0SWXCF1ccbfJOINcFG5SuUbgkYURgpEpk7w0FxNgBcnkvOMb9JDWTBsLc8YBDydDm5J4ps6R6LJO1u0gIKnxqCRrnMkaWt9o8F4niPaWpqHSAyvEbnZsl9g4IP/AF2SGMxNd4S7ORvLlTsEs+goKpr2hw2HUcwpcy+c4e0lWTpNNbgHmwCu8N7ZV0bgRKX7srjmbZd2MNnuN1y6xOFekGFwAqGGI21ePE0laCDtFRvLQyoicXEBrQ7UnohTRxbErl1xJAIiVxJJcccTXJxTSuAMKjcpHFROKVjEbl1NcUkoQWgGruoV4xUlB7TvJXrAoYtMtMcFPDtPRQIiHb5K0dkZaJCo5zZp38uSlIUVT7JVBBsbri28b+ISdsQ0Tv13f4XMTrBFC+XaA0uI4jgkaCjD9v8AtYacGKItLnAtP4mHovHZn6orHMUdU1Ekr9C52zgNwQFQCQOK0QjSFkx4qQP0+KEklLiSVyxPFcYw7bKgCdrXW0IATTKW6A3XHRnyTe7Nx1ROD6arsRcklaTAcSZFLG86WcC47bBYwHU/vRWEE2gHH4BBo4+icMxCOduaNxe38eUgE+aOsvNPRnXVbgI7OdTi9nEaB2+xXpig1TGG2STlwoBGFMKe5RuKBwxxUbinOKicUgxG8pJkrkktjpDKL7R3RXzNg6KjpR6w+6rxmwdFLH7HnpDrKeHb/aoQpoto6K0dkZaJlBVGzHE7LfBToTFdIJSNuQlVlwrJx3QICqrtnPGygqXyh7mCM3a02JduReHyFwAPC6qfSIz/AKZV/wBMfMKcZWVlGmfPj363RNIHO2DTchQy5A52W4wzCmtjbp90fqr5J9qOxY+9mfioXH7pvuI4qzpMCe/Ui3NaOhom5lo46Rotoszzy9Gr/ngtmVw3sY0m8rszdzQLI6TsdTbmO/5LVQsARcUYO0JP1Jv2BwgvR5bj/YssaZIcxIFyw7wsdDtFyQWm1uS+ijTAi1l4129wgU1ZJlAtIM4Ftl1qwzb4ZmyxjtFl6McYdHVNhdI5sb3FoBPgc7cvbLLwb0b4dLJVxkMLog/1pLfBp9di95TT2TRxNcnFMckCMco3lSOUMhSyCiNxUEjuCfI5R2U2x0MskpLJIDDYftf7VeRbB0VJH9q3oVdw+yOiSG2GWkPspoto6KIKWPaOhVY7IyJkNiQ9TL/Td8kSoawXjkH5HfJVlpiR2jN4Q894W7sgcOSscSohPDLCdkkboz5iyrMKHrx/SCu3yBoLnEADUuJ0AWeD4NOTZ8zQUREhZY5mvLLfmBsvQKaPIxoJGgGqrsSpGsxafIWuje908TgcwLXa/O6mxaO9g5+RnHiU+R9zSHwrtRdUbI73MjR5q8ZCCBlcD5rzf/SRMD3Mj3vbqW6DToSiuzeJOik7t5dm2ZTpZTePix1Nt0elxUhA1RUMG+6Fjkc+HOSBpxWWnqZnS2iqQ11+otzRikI+5m/hasJ6UcLztbKB4g23ldabCny5QXva9w2kbCU/tLGHwgHaXANHNXg6ZCRQeiLDZI4JpH6MkkHdt5gWJ/fBb4hC4XTtghjiaLBrbee0ogvCaTtk0IphXS8KJ8iVsJyRyGeU5zrppCm3Y6VEZCbZSFNIQCIJKSKEnpxXUaA2D/8Acb5q6p/ZCpX/AGjOquqX2QpxXyY8vFEoUjNo800J8e7qVaJGRMVHOPA/3XfJSJsg8J90p2TRlsMHrm/0vqgfSDI7uYYmkgyTakfhaL/4Vhh49cz+mUH28jPdQvA1bI4X5lpA+Ky/U3Q/kVmAbEx1U0ttmaHtfbjptRmIYeJbXvbgDZA0hayVm0OLiyUH8ThcfJaykYLJbqmXcVbMjBhbY5BJkfdoAac2haNx4+ahiw0vqDKfCC7MG7geXALaYi1jI3PIGz4qtw6nuQ6S2urW8kznKqCoR3RraSnvAG7rbOSy8nZaVtTHJG8xtabuNiWya77HRbOgpwGixGm6+9Ehl9R5jgVSPCMspcsraOgc2UvLgW/cFrPaOBP3h11U1fAXywi12tJlcL7dwVi2JBTzlkrdDlPgc78O0/QJrrkSrdB1K8StztNxci/MGxUvcHmh8IiLWOB/3pHD3bo6xXKQkopPghFMoKunDW352R1kNXjwj3kWwLYBZcIUjWk6AEngjIMP3v8A+ISqLYzaRXtZcoiOADU6n4J8rAJHACwsNF2yZIDdjxZdTAEk1ilXUaPZ7yuqb2R+9LqlrBq33wrql9ked1BeTKy8UTBPZu6poCe36q8SLJEjsPRdSTMRGYoR66P3HBWddRsmjdE8Xa4WPI8VW02k8fSQfFXYWaKNMnTTPN+0HZSeON892BsTmyXvcyAHcNyioKon/C9GxmDvKaaP8TXMHUjReVYXUBpY87vaHA7wjOCpFsWRyuwbEe0vfu7qNhc0HU8bIWhpZnzMcWubl8TSXHwjkE6PB4pSXgvjcHkgtdY5b6K6w7D4gQHSStN/C7PpfzXcLhFVdcmlw94eyPP3zSHZgQ4szO58QryN+u29wgaKBwb4Zc3UXQrHzmqaHRBrANJA/MHeW5MkZpPkvnyoBrO+YDc5Sc9htc6/Hgn1ps0nlp1QYqHtFmGwuDt321Rq0IpVyaSnZZoB27T1UllmIqqV17yP8nIHG6qZsRImladlw8hLdegqFtc7NsoaiPNkadAXbfJLDQe5iuST3TCSdpNgm3PfgXNg29ueqoS9hUULWiwFuaeuqk7RvcMuUkeEnQ2TOXarFgu50TzfaP8AJOAQWD3MbSSScrbknUlHgJE75HargQCSeAkuOKWuGz3grei2HqqqvGnmFa0Wwqf3H+gSnN+q5ZOarkWSJBJIIimbj+3j96QfFXVlUgeuZ/Uk+auFCJefo5N7B6heEY0TBVTAEhplkc3ha50XvE3sHqF4tj0IldLv9Y8g87lPJ1VjYU3dFfSYsGuGhsdvVauga2obdptvOi88EZYbODiOI2q7oMXMbLRucDe1su1qEoJ8oosjSpnqlCcjRcjZt2BSzTi4N9m7msRRY++QBvjJtq0MJV/h1LNIbvuxm+58Tj9EVGtkpO2WweX67t3VRvZ9N3JGmMNAaBYAaKCYanr9E6Jt8kULNnMIPtNF/LOP5gPmrKMex0UHadv8qffH1SNcSHi/lE0lELRRjhGz5BQgev8A7ERALMZ7rR8FCz7Y+6mfon7YWqTtCDp7h+au1T45t/sQyaDi8hmEN9W33W/JHAIbC2+AeXyRoCEVwGT5OAJJ1kk1C2UdeNCrOgPhPkq+vHhPRHYYdP7QVL7lPoHBdC4F0K5EkKQXCbXJsBtJ4BYztL6RKWlzMh/mZRpYG0TTzdv8k0YOTpC2WdQ8MlBcQAJX3JNgAs/6QO18cUDoaSdjql4OrHZjHGPaNxvts815njeP1NY9z5pDYuLmxN8MbDyCppGHa02cDdpHFWh0jStjyypm/wAE7U1UtIYX1L5LN8TnW7y1xoXbwmsZdYakrXRudIweEgtmjH3Cd/T/AOLX4PXskaCCL7xzWHPjlF36NuCcWqWyKopAHC43rX4HSxuYPC0kbTZVFVDoHaG23orzs9HYEjYdo5qXcVklRewQNGwAeSKYLKEGybNWRxNLpHBoG8lVjb4Rkf5KH0hEinuyonglBtGI3270Ei4I6b9y72ZxAuiZFM+87WB5B9own2SeJta6qcUrGzy/xUrT3LBkp4joZDfU9OPILPOr3PqhJfxE7dmll62Lpbx09mGeX5cHq0X3OiH7Sn+Wt+cb9yw9J2nkjIBebDiMwV47G46mMsc8xyAhzWkXZJ0O5ZcvS5IJ8XZfHki2nej0Vg0HQIaL7Z3u/wCERDI1zQ5pDmkaOBuChoftpOihJaOXsMVRjO3+1W6rMTpnvJytJ8NvNCStBg6Z3DR4P3wRgCHw8WYBzRYCMdHSfJwBJOST0IUVaND0RWEnwj3QoKwaFPwuQNZmcQAGFzidwCzy80XXgyykkawFziGtGpcTYALF476R6aG7KdpnfszezGD13rDdtu1D62ZwY5zadvhjZfR/5iOazIC9TD0vcrkZpTov8d7Y1lXdskmWP/aZ4GEc+Pms4832pxXcml1ujijHSJOTI2m5I4C6jI1UkAu5w/L8U5rB5rqtHAb2lpzNNiNOo4Fdoq0xPzNGQ/ejv4HdOCLfDdDy042HyKhlwdw8MjRuJJ54P4QSNjeyoOR5Dj6t+mnPQ3WiikMDTqRbivLaN9QHwBhfLklYYIic2V99gHAr0PHMSDmFjo3RTWBfEdSAdhBGhB3ELyepwfp1weh0+XvbTK/F+1tQSI6e5eT7QFz5KM1wDI5Zj385aCWueTEw8TxOzQaKmYQH5gSZAC1o2WBFiT5XUcwLiGNN3HT3QvR6Hpko98tmTq8ly7VoKkxCSpkJc45WjU7ug4BCRPJnZ7wARTIcje7bb8x4uSbA1r4he5MgPQL0U64MdEdZq91uOvVcZOWgX1HyKLrGgXttJKrKqSzbc/1KOwmhwXtDNA4Fkr2DkbtI5tOh+a9H7OdoWTOPeFrXuAAI9hx+i8cEWRjS72jrb8IVlhlW5p0JCzZumjNcbHjNo9wrXaDjfbyUDpXttY38NyCLrM9lsbfOe6kOazfVu+i0csg16ZQeQ2ryMsHB9rNWOmrHU0nh8yp46ppOUmxtfXghqc+EKOofr5fFTUqD22Wo8klQYWc8xGZ1iwvFnb72XEVNPk6cHHgmq9hWW7Z4g6HDyGmxld3B45dp+S1dSNPJeb+k2e0NLGDtfI/yAA+q6EbyxGuoMwJksQTsvY9EX3etuV1XVI0vuI/Qq0p35o43b8tndV7uPh0YpfkieyxsmzCwC7O+3UpVZ2dFUUHo/tD0UpZZxUVKbOB/NZFTN8aSGhpDg1Qzs2AopD1Wlk7FFCXNIc1xa9pDmO4OGxEw1c9dVRvmfYxNLZ3NaGAU412Dz+CHaCRorrsXjP8ABCuc6JszXR+NhNiQCAB08WoWfNjvmrHjKvZWsJeXFmgLvaO3LuRkLGsGm3e7eVX0zyWtA4bBuR8TLC5utHpCDw6yZA68zTwXM6bh5vITwC44KrnWuQqrDYu8mF9gN7KyqBe97b9OaGwNtpHFd6OJKsZnkn2QbAcSmUji4uDeOVoHFOrvCHcLk+an7JM8MkztjXeEcZCg3Rxr+zrDG4A7Q0ErYtfexuNRoOSwWF1ZMszjsbFr1K1eBz95TQPvtZr8vovL62OpF8D2i6idoFFbO63E2UDZDe19N6bK9/eRhhA8WY6beS880rgH7Kh/fSMeHNMbO7PNwP8AhJEx94Zah7YgJDE1sb7276QH4dV1GMaQc0u+VrgImf4fJeTek0uE8LSCAIy4dCf/AEvWms3rzD0sRXqIzv7jT/kVbpallRPJ8YMwjLHM07LZgeCIwuSwdGdoNx0QLJCLOG7aOSeJckjSPZcLA8uHkvZg9MyMJqza3VPqn3t0UVQL/veuZrgJ3toAxxs0H811a1Q8bSOAPkqeqd4VdPN2Ru4tGq6O6OerGk7kNWjRTb1FWjwJpaAjuGgkga7Vo8DpKN/8cypmMDsngJeGju7anmbgaKi7PMvcncuVD/WVX/jSfrop5LcOGFbO4C3Mwk6gaXVk43CrezLrxSjeDfyRMUlwQmjyjnsbIba/JNw37552XZ/ZKdQM8HU380wAjXVdw+IiTjccE+Mc/JGYaAZgLjQXJ5INnFNj7wGO2g7uqlwqUNhjbsa1vePP4pHbP0HzU3bSEDI1v3na9FU1lR4GxR3u45WjqgueTmX1BU93RzynbI5zr/kGgW47Juy0dKDtMQd+uq8yxubLFHTM2nLE22/9leq00XdwwtbtYxrB1AssXXcYyuDyDgdSnXve48iqenxF/eWc0242Vux4IuDcLx0za1RNBO5h3ubvF/EByKSiSTqbJuIdfdtPBedelFnrKc3BJZIwjhqD9V6FIbDT9V596SLZYTv7xwv5KvR/HNGzsvygzzGdhY6+4rnhcCDe23oeIRlS0EW37kBq3xDzHJe5KNP+jCnwT00pIIJuRoTxHFOYfmq90mR+Yey75I1rgUinzX4HcSOrdpZX0JHcR8Q1ZqodqAtBTn1AHArsbuTBJUkcaLofE32aB+7ophsFV1j87w3ns5K03URVs0OBRZYSeOpV2OwNaQ+QOpnCSnc1re9LXXcBa9wqsgMgDR+HVa+i9IFH3bA5lWCGtFsjXhpA3arLneRJKCspBRb5MVTYLUUNR3FQwNMkedhDw8OAQrDZ7huurbtl2iiqq2kkiMhaxhjfmZkNy66qMRGWZ3C9/JU6dycflsE6vgv+z2ENq5RE9zmsLXPLm7QANPitRB2ZZStBIiqIy2z45I8r7HTwuGw3IXnsLycoBcNdCDZaBuM1DAGNmlAGUZSczbC1tvQKefHkb+MuAwa9o03aLB5XBjKeGIRMaPACzOx3EO2m42rEYpE6COZsjXMkzZMp9phGq1LO01VttFJsu0sy3GnDosR2+xN7pGOI8chkkcL3a03S4e+Cqegyp6BMRxfPHEH6yMblJvtbxKHwy5c6Z2lhaO+48VSxEk2Bu47XHcrGqnDIw1p3WutMJWr9Ik16LbstCaqvY43LIvWm/LYvX6YXYL8SvP8AsBQGKldMR4pXZW8cgXoWGG8QJWDq+YNlsPEgSSAa7lHFKYzy4KxlZwQc0d15DVG1Ow2GYPFx5jgkqgZmm428Ul3cd2mnk2LB+keK9O134ZW/oQkktON/uw/0j9WecSm4224FBS6HUW57ikkvopaMCA6ht78PkVyjm+6fJJJefkdTLx0dOrx1Wkoz6ojn8Eklfp9sXJ6B6qWzULhEZkmB5pJJ8j+SQq0zRYm+zSNmipKLUkJJKi9CkVeC17TwIKvcZjzMil/E0X6rqSVeTGegbCTmkY3mrrEY8st9RcbUkkZbOQTkNhY7Vke3QdmguDbK8ZraE3Gi4koZfEeOynp7MHM+0eAUlHAaiZkY1u4DySSXN6j6F/LPXHMEfdQtHhY0DzWjwp12WHH4pJLN1X8Y+LyC3BCysSSXks2IEkYkkkkHs//Z"
                alt=""
              />
              <MDBCardBody>
                <h4 className="card-title">Raj Shah</h4>
                <hr />
                <p>
                  <MDBIcon icon="quote-left" /> Lorem ipsum dolor sit amet,
                  consectetur adipisicing elit. Eos, adipisci{" "}
                  <MDBIcon icon="quote-right" />
                </p>
              </MDBCardBody>
            </div>
          </MDBCard>
        </MDBCol>

        <MDBCol md="6">
          <MDBCard>
            <div className="d-flex p-2">
              <img
                style={{
                  borderRadius: "50%",
                  width: "100%",
                  width: 120,
                  height: 120,
                  alignSelf: "center",
                }}
                src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxAQEBAQEBANEBAVEBINDQ0VDRsQEA4NIB0iIiAdHx8kKDQsJCYxJx8fLTItMT1AMEMwIys/QD8uTDRBMDcBCgoKDQ0OFQ8QFTcZFRkrKy03KzcxNzc3Kys3LSs3Nzc3Ky0rKys3LSsrLTg3My0rKysrKzctLSstKy0rKysrK//AABEIAMgAyAMBIgACEQEDEQH/xAAcAAABBQEBAQAAAAAAAAAAAAAEAAIDBQYBBwj/xAA/EAABAwIDBQQHBgUDBQAAAAABAAIDBBEFEiExQVFhcQYTIoEHIzJyobHBM0JSYpHwFCRzgtEVU5IlQ3Th8f/EABoBAAMBAQEBAAAAAAAAAAAAAAECAwQABQb/xAAnEQACAgIBBAIBBQEAAAAAAAAAAQIRAzEhBBIyQSJCURMjM2FxFP/aAAwDAQACEQMRAD8A1+DNtmH57rQgKjw0au94K+C8zFo1ZNiARMO0qABEwjU9FZbJS0SWXCF1ccbfJOINcFG5SuUbgkYURgpEpk7w0FxNgBcnkvOMb9JDWTBsLc8YBDydDm5J4ps6R6LJO1u0gIKnxqCRrnMkaWt9o8F4niPaWpqHSAyvEbnZsl9g4IP/AF2SGMxNd4S7ORvLlTsEs+goKpr2hw2HUcwpcy+c4e0lWTpNNbgHmwCu8N7ZV0bgRKX7srjmbZd2MNnuN1y6xOFekGFwAqGGI21ePE0laCDtFRvLQyoicXEBrQ7UnohTRxbErl1xJAIiVxJJcccTXJxTSuAMKjcpHFROKVjEbl1NcUkoQWgGruoV4xUlB7TvJXrAoYtMtMcFPDtPRQIiHb5K0dkZaJCo5zZp38uSlIUVT7JVBBsbri28b+ISdsQ0Tv13f4XMTrBFC+XaA0uI4jgkaCjD9v8AtYacGKItLnAtP4mHovHZn6orHMUdU1Ekr9C52zgNwQFQCQOK0QjSFkx4qQP0+KEklLiSVyxPFcYw7bKgCdrXW0IATTKW6A3XHRnyTe7Nx1ROD6arsRcklaTAcSZFLG86WcC47bBYwHU/vRWEE2gHH4BBo4+icMxCOduaNxe38eUgE+aOsvNPRnXVbgI7OdTi9nEaB2+xXpig1TGG2STlwoBGFMKe5RuKBwxxUbinOKicUgxG8pJkrkktjpDKL7R3RXzNg6KjpR6w+6rxmwdFLH7HnpDrKeHb/aoQpoto6K0dkZaJlBVGzHE7LfBToTFdIJSNuQlVlwrJx3QICqrtnPGygqXyh7mCM3a02JduReHyFwAPC6qfSIz/AKZV/wBMfMKcZWVlGmfPj363RNIHO2DTchQy5A52W4wzCmtjbp90fqr5J9qOxY+9mfioXH7pvuI4qzpMCe/Ui3NaOhom5lo46Rotoszzy9Gr/ngtmVw3sY0m8rszdzQLI6TsdTbmO/5LVQsARcUYO0JP1Jv2BwgvR5bj/YssaZIcxIFyw7wsdDtFyQWm1uS+ijTAi1l4129wgU1ZJlAtIM4Ftl1qwzb4ZmyxjtFl6McYdHVNhdI5sb3FoBPgc7cvbLLwb0b4dLJVxkMLog/1pLfBp9di95TT2TRxNcnFMckCMco3lSOUMhSyCiNxUEjuCfI5R2U2x0MskpLJIDDYftf7VeRbB0VJH9q3oVdw+yOiSG2GWkPspoto6KIKWPaOhVY7IyJkNiQ9TL/Td8kSoawXjkH5HfJVlpiR2jN4Q894W7sgcOSscSohPDLCdkkboz5iyrMKHrx/SCu3yBoLnEADUuJ0AWeD4NOTZ8zQUREhZY5mvLLfmBsvQKaPIxoJGgGqrsSpGsxafIWuje908TgcwLXa/O6mxaO9g5+RnHiU+R9zSHwrtRdUbI73MjR5q8ZCCBlcD5rzf/SRMD3Mj3vbqW6DToSiuzeJOik7t5dm2ZTpZTePix1Nt0elxUhA1RUMG+6Fjkc+HOSBpxWWnqZnS2iqQ11+otzRikI+5m/hasJ6UcLztbKB4g23ldabCny5QXva9w2kbCU/tLGHwgHaXANHNXg6ZCRQeiLDZI4JpH6MkkHdt5gWJ/fBb4hC4XTtghjiaLBrbee0ogvCaTtk0IphXS8KJ8iVsJyRyGeU5zrppCm3Y6VEZCbZSFNIQCIJKSKEnpxXUaA2D/8Acb5q6p/ZCpX/AGjOquqX2QpxXyY8vFEoUjNo800J8e7qVaJGRMVHOPA/3XfJSJsg8J90p2TRlsMHrm/0vqgfSDI7uYYmkgyTakfhaL/4Vhh49cz+mUH28jPdQvA1bI4X5lpA+Ky/U3Q/kVmAbEx1U0ttmaHtfbjptRmIYeJbXvbgDZA0hayVm0OLiyUH8ThcfJaykYLJbqmXcVbMjBhbY5BJkfdoAac2haNx4+ahiw0vqDKfCC7MG7geXALaYi1jI3PIGz4qtw6nuQ6S2urW8kznKqCoR3RraSnvAG7rbOSy8nZaVtTHJG8xtabuNiWya77HRbOgpwGixGm6+9Ehl9R5jgVSPCMspcsraOgc2UvLgW/cFrPaOBP3h11U1fAXywi12tJlcL7dwVi2JBTzlkrdDlPgc78O0/QJrrkSrdB1K8StztNxci/MGxUvcHmh8IiLWOB/3pHD3bo6xXKQkopPghFMoKunDW352R1kNXjwj3kWwLYBZcIUjWk6AEngjIMP3v8A+ISqLYzaRXtZcoiOADU6n4J8rAJHACwsNF2yZIDdjxZdTAEk1ilXUaPZ7yuqb2R+9LqlrBq33wrql9ked1BeTKy8UTBPZu6poCe36q8SLJEjsPRdSTMRGYoR66P3HBWddRsmjdE8Xa4WPI8VW02k8fSQfFXYWaKNMnTTPN+0HZSeON892BsTmyXvcyAHcNyioKon/C9GxmDvKaaP8TXMHUjReVYXUBpY87vaHA7wjOCpFsWRyuwbEe0vfu7qNhc0HU8bIWhpZnzMcWubl8TSXHwjkE6PB4pSXgvjcHkgtdY5b6K6w7D4gQHSStN/C7PpfzXcLhFVdcmlw94eyPP3zSHZgQ4szO58QryN+u29wgaKBwb4Zc3UXQrHzmqaHRBrANJA/MHeW5MkZpPkvnyoBrO+YDc5Sc9htc6/Hgn1ps0nlp1QYqHtFmGwuDt321Rq0IpVyaSnZZoB27T1UllmIqqV17yP8nIHG6qZsRImladlw8hLdegqFtc7NsoaiPNkadAXbfJLDQe5iuST3TCSdpNgm3PfgXNg29ueqoS9hUULWiwFuaeuqk7RvcMuUkeEnQ2TOXarFgu50TzfaP8AJOAQWD3MbSSScrbknUlHgJE75HargQCSeAkuOKWuGz3grei2HqqqvGnmFa0Wwqf3H+gSnN+q5ZOarkWSJBJIIimbj+3j96QfFXVlUgeuZ/Uk+auFCJefo5N7B6heEY0TBVTAEhplkc3ha50XvE3sHqF4tj0IldLv9Y8g87lPJ1VjYU3dFfSYsGuGhsdvVauga2obdptvOi88EZYbODiOI2q7oMXMbLRucDe1su1qEoJ8oosjSpnqlCcjRcjZt2BSzTi4N9m7msRRY++QBvjJtq0MJV/h1LNIbvuxm+58Tj9EVGtkpO2WweX67t3VRvZ9N3JGmMNAaBYAaKCYanr9E6Jt8kULNnMIPtNF/LOP5gPmrKMex0UHadv8qffH1SNcSHi/lE0lELRRjhGz5BQgev8A7ERALMZ7rR8FCz7Y+6mfon7YWqTtCDp7h+au1T45t/sQyaDi8hmEN9W33W/JHAIbC2+AeXyRoCEVwGT5OAJJ1kk1C2UdeNCrOgPhPkq+vHhPRHYYdP7QVL7lPoHBdC4F0K5EkKQXCbXJsBtJ4BYztL6RKWlzMh/mZRpYG0TTzdv8k0YOTpC2WdQ8MlBcQAJX3JNgAs/6QO18cUDoaSdjql4OrHZjHGPaNxvts815njeP1NY9z5pDYuLmxN8MbDyCppGHa02cDdpHFWh0jStjyypm/wAE7U1UtIYX1L5LN8TnW7y1xoXbwmsZdYakrXRudIweEgtmjH3Cd/T/AOLX4PXskaCCL7xzWHPjlF36NuCcWqWyKopAHC43rX4HSxuYPC0kbTZVFVDoHaG23orzs9HYEjYdo5qXcVklRewQNGwAeSKYLKEGybNWRxNLpHBoG8lVjb4Rkf5KH0hEinuyonglBtGI3270Ei4I6b9y72ZxAuiZFM+87WB5B9own2SeJta6qcUrGzy/xUrT3LBkp4joZDfU9OPILPOr3PqhJfxE7dmll62Lpbx09mGeX5cHq0X3OiH7Sn+Wt+cb9yw9J2nkjIBebDiMwV47G46mMsc8xyAhzWkXZJ0O5ZcvS5IJ8XZfHki2nej0Vg0HQIaL7Z3u/wCERDI1zQ5pDmkaOBuChoftpOihJaOXsMVRjO3+1W6rMTpnvJytJ8NvNCStBg6Z3DR4P3wRgCHw8WYBzRYCMdHSfJwBJOST0IUVaND0RWEnwj3QoKwaFPwuQNZmcQAGFzidwCzy80XXgyykkawFziGtGpcTYALF476R6aG7KdpnfszezGD13rDdtu1D62ZwY5zadvhjZfR/5iOazIC9TD0vcrkZpTov8d7Y1lXdskmWP/aZ4GEc+Pms4832pxXcml1ujijHSJOTI2m5I4C6jI1UkAu5w/L8U5rB5rqtHAb2lpzNNiNOo4Fdoq0xPzNGQ/ejv4HdOCLfDdDy042HyKhlwdw8MjRuJJ54P4QSNjeyoOR5Dj6t+mnPQ3WiikMDTqRbivLaN9QHwBhfLklYYIic2V99gHAr0PHMSDmFjo3RTWBfEdSAdhBGhB3ELyepwfp1weh0+XvbTK/F+1tQSI6e5eT7QFz5KM1wDI5Zj385aCWueTEw8TxOzQaKmYQH5gSZAC1o2WBFiT5XUcwLiGNN3HT3QvR6Hpko98tmTq8ly7VoKkxCSpkJc45WjU7ug4BCRPJnZ7wARTIcje7bb8x4uSbA1r4he5MgPQL0U64MdEdZq91uOvVcZOWgX1HyKLrGgXttJKrKqSzbc/1KOwmhwXtDNA4Fkr2DkbtI5tOh+a9H7OdoWTOPeFrXuAAI9hx+i8cEWRjS72jrb8IVlhlW5p0JCzZumjNcbHjNo9wrXaDjfbyUDpXttY38NyCLrM9lsbfOe6kOazfVu+i0csg16ZQeQ2ryMsHB9rNWOmrHU0nh8yp46ppOUmxtfXghqc+EKOofr5fFTUqD22Wo8klQYWc8xGZ1iwvFnb72XEVNPk6cHHgmq9hWW7Z4g6HDyGmxld3B45dp+S1dSNPJeb+k2e0NLGDtfI/yAA+q6EbyxGuoMwJksQTsvY9EX3etuV1XVI0vuI/Qq0p35o43b8tndV7uPh0YpfkieyxsmzCwC7O+3UpVZ2dFUUHo/tD0UpZZxUVKbOB/NZFTN8aSGhpDg1Qzs2AopD1Wlk7FFCXNIc1xa9pDmO4OGxEw1c9dVRvmfYxNLZ3NaGAU412Dz+CHaCRorrsXjP8ABCuc6JszXR+NhNiQCAB08WoWfNjvmrHjKvZWsJeXFmgLvaO3LuRkLGsGm3e7eVX0zyWtA4bBuR8TLC5utHpCDw6yZA68zTwXM6bh5vITwC44KrnWuQqrDYu8mF9gN7KyqBe97b9OaGwNtpHFd6OJKsZnkn2QbAcSmUji4uDeOVoHFOrvCHcLk+an7JM8MkztjXeEcZCg3Rxr+zrDG4A7Q0ErYtfexuNRoOSwWF1ZMszjsbFr1K1eBz95TQPvtZr8vovL62OpF8D2i6idoFFbO63E2UDZDe19N6bK9/eRhhA8WY6beS880rgH7Kh/fSMeHNMbO7PNwP8AhJEx94Zah7YgJDE1sb7276QH4dV1GMaQc0u+VrgImf4fJeTek0uE8LSCAIy4dCf/AEvWms3rzD0sRXqIzv7jT/kVbpallRPJ8YMwjLHM07LZgeCIwuSwdGdoNx0QLJCLOG7aOSeJckjSPZcLA8uHkvZg9MyMJqza3VPqn3t0UVQL/veuZrgJ3toAxxs0H811a1Q8bSOAPkqeqd4VdPN2Ru4tGq6O6OerGk7kNWjRTb1FWjwJpaAjuGgkga7Vo8DpKN/8cypmMDsngJeGju7anmbgaKi7PMvcncuVD/WVX/jSfrop5LcOGFbO4C3Mwk6gaXVk43CrezLrxSjeDfyRMUlwQmjyjnsbIba/JNw37552XZ/ZKdQM8HU380wAjXVdw+IiTjccE+Mc/JGYaAZgLjQXJ5INnFNj7wGO2g7uqlwqUNhjbsa1vePP4pHbP0HzU3bSEDI1v3na9FU1lR4GxR3u45WjqgueTmX1BU93RzynbI5zr/kGgW47Juy0dKDtMQd+uq8yxubLFHTM2nLE22/9leq00XdwwtbtYxrB1AssXXcYyuDyDgdSnXve48iqenxF/eWc0242Vux4IuDcLx0za1RNBO5h3ubvF/EByKSiSTqbJuIdfdtPBedelFnrKc3BJZIwjhqD9V6FIbDT9V596SLZYTv7xwv5KvR/HNGzsvygzzGdhY6+4rnhcCDe23oeIRlS0EW37kBq3xDzHJe5KNP+jCnwT00pIIJuRoTxHFOYfmq90mR+Yey75I1rgUinzX4HcSOrdpZX0JHcR8Q1ZqodqAtBTn1AHArsbuTBJUkcaLofE32aB+7ophsFV1j87w3ns5K03URVs0OBRZYSeOpV2OwNaQ+QOpnCSnc1re9LXXcBa9wqsgMgDR+HVa+i9IFH3bA5lWCGtFsjXhpA3arLneRJKCspBRb5MVTYLUUNR3FQwNMkedhDw8OAQrDZ7huurbtl2iiqq2kkiMhaxhjfmZkNy66qMRGWZ3C9/JU6dycflsE6vgv+z2ENq5RE9zmsLXPLm7QANPitRB2ZZStBIiqIy2z45I8r7HTwuGw3IXnsLycoBcNdCDZaBuM1DAGNmlAGUZSczbC1tvQKefHkb+MuAwa9o03aLB5XBjKeGIRMaPACzOx3EO2m42rEYpE6COZsjXMkzZMp9phGq1LO01VttFJsu0sy3GnDosR2+xN7pGOI8chkkcL3a03S4e+Cqegyp6BMRxfPHEH6yMblJvtbxKHwy5c6Z2lhaO+48VSxEk2Bu47XHcrGqnDIw1p3WutMJWr9Ik16LbstCaqvY43LIvWm/LYvX6YXYL8SvP8AsBQGKldMR4pXZW8cgXoWGG8QJWDq+YNlsPEgSSAa7lHFKYzy4KxlZwQc0d15DVG1Ow2GYPFx5jgkqgZmm428Ul3cd2mnk2LB+keK9O134ZW/oQkktON/uw/0j9WecSm4224FBS6HUW57ikkvopaMCA6ht78PkVyjm+6fJJJefkdTLx0dOrx1Wkoz6ojn8Eklfp9sXJ6B6qWzULhEZkmB5pJJ8j+SQq0zRYm+zSNmipKLUkJJKi9CkVeC17TwIKvcZjzMil/E0X6rqSVeTGegbCTmkY3mrrEY8st9RcbUkkZbOQTkNhY7Vke3QdmguDbK8ZraE3Gi4koZfEeOynp7MHM+0eAUlHAaiZkY1u4DySSXN6j6F/LPXHMEfdQtHhY0DzWjwp12WHH4pJLN1X8Y+LyC3BCysSSXks2IEkYkkkkHs//Z"
                alt=""
              />
              <MDBCardBody>
                <h4 className="card-title">Raj Shah</h4>
                <hr />
                <p>
                  <MDBIcon icon="quote-left" /> Lorem ipsum dolor sit amet,
                  consectetur adipisicing elit. Eos, adipisci{" "}
                  <MDBIcon icon="quote-right" />
                </p>
              </MDBCardBody>
            </div>
          </MDBCard>
        </MDBCol>
      </MDBRow> */}
    </div>
  );
};

export default About;
