import React from "react";

import UserLayout from "./UserLayout";

import { CardElement, useStripe, useElements } from "@stripe/react-stripe-js";

import { MDBBtn } from "mdbreact";

import { Formik, Field } from "formik";
import InputField from "../../../components/form/inputField";

import classes from "./common.module.scss";

import axios from "axios";
import { BASE_URL } from "../../../constants";
import { toastError } from "../../../utils";
const UserBilling = () => {
  const stripe = useStripe();

  const elements = useElements();
  console.log("stripe", elements, stripe);
  if (stripe || elements) {
    // Stripe.js has not loaded yet. Make sure to disable
    // form submission until Stripe.js has loaded.
  }

  const submitStipe = async (e) => {
    e.preventDefault();
    console.log("stripe", stripe);

    if (!stripe || !elements) {
      // Stripe.js has not loaded yet. Make sure to disable
      // form submission until Stripe.js has loaded.

      return;
    }

    const cardElement = elements.getElement(CardElement);

    const { error, paymentMethod } = await stripe.createPaymentMethod({
      type: "card",
      card: { ...cardElement },
    });

    if (error) {
      console.log("[error]", error);
    } else {
      console.log("[PaymentMethod]", paymentMethod);
    }
  };

  return (
    <UserLayout>
      <Formik initialValues={{}} validationSchema={{}} onSubmit={submitStipe}>
        {({
          isValid,
          isSubmitting,
          handleSubmit,
          values,
          touched,
          handleChange,
          handleBlur,
        }) => (
          <form
            method="POST"
            className="w-100 mt-2 custom-input-wrapper"
            onSubmit={handleSubmit}
          >
            <div className="row ml-3 mt-4 pt-4 col-10 p-0">
              <form
                className="w-100 col-sm-12 col-md-8"
                onSubmit={handleSubmit}
              >
                <div className="row m-0">
                  <div
                    className={`col-12 pb-4 px-0 pl-1 ${classes.paragraphTitle}`}
                  >
                    Billing information
                  </div>
                  <div className="col-sm-12 col-md-6 pl-md-0 pr-md-1">
                    <Field
                      autoComplete="off"
                      className="m-0"
                      type="text"
                      name="firstName"
                      label="First name"
                      touched={touched}
                      component={InputField}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      value={values.email}
                      variant="outlined"
                    />
                  </div>
                  <div className="col-sm-12 col-md-6 pr-md-0 pl-md-1">
                    <Field
                      autoComplete="off"
                      className="m-0"
                      type="text"
                      name="lastName"
                      label="Last name"
                      touched={touched}
                      component={InputField}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      value={values.lastName}
                      variant="outlined"
                    />
                  </div>
                  <div className="col-sm-12 col-md-6 pl-md-0 pr-md-1">
                    <Field
                      autoComplete="off"
                      className="m-0"
                      type="text"
                      name="companyName"
                      label="Company name"
                      touched={touched}
                      component={InputField}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      value={values.companyName}
                      variant="outlined"
                    />
                  </div>
                  <div className="col-sm-12 col-md-6 pr-md-0 pl-md-1">
                    <Field
                      autoComplete="off"
                      className="m-0"
                      type="text"
                      name="vatNumber"
                      label="VAT Number"
                      touched={touched}
                      component={InputField}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      value={values.vatNumber}
                      variant="outlined"
                    />
                  </div>
                  <div className="col-12 px-md-0">
                    <Field
                      autoComplete="off"
                      className="m-0"
                      type="text"
                      name="address"
                      label="Address"
                      touched={touched}
                      component={InputField}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      value={values.address}
                      variant="outlined"
                    />
                  </div>
                  <div className="col-sm-12 col-md-6 pl-md-0 pr-md-1">
                    <Field
                      autoComplete="off"
                      className="m-0"
                      type="text"
                      name="zipCode"
                      label="ZIP code"
                      touched={touched}
                      component={InputField}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      value={values.zipCode}
                      variant="outlined"
                    />
                  </div>
                  <div className="col-sm-12 col-md-6 pr-md-0 pl-md-1">
                    <Field
                      autoComplete="off"
                      className="m-0"
                      type="text"
                      name="city"
                      label="City"
                      touched={touched}
                      component={InputField}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      value={values.city}
                      variant="outlined"
                    />
                  </div>
                  <div className="col-12"></div>

                  <div className={`col-12 px-0 pl-1 ${classes.paragraphTitle}`}>
                    Payment
                  </div>
                  <div
                    className={`col-12 pb-4 px-0 pl-1 ${classes.paragraphSubTitleSettings}`}
                  >
                    Powered by <span className="stripe">stripe</span>
                  </div>
                </div>
                <CardElement />
                <MDBBtn
                  className="blueBtn m-0 mt-4 px-3 py-2"
                  onClick={submitStipe}
                  disabled={!stripe}
                >
                  Pay
                </MDBBtn>
              </form>
            </div>
          </form>
        )}
      </Formik>
    </UserLayout>
  );
};

export default UserBilling;
