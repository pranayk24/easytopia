import React, { Suspense } from "react";

// routing
import { Switch, Route } from "react-router-dom";

// Routes
import routes from "./routes";

// Private routing
import PrivateRoute from "../components/PrivateRoute";

const CenterText = ({ text }) => (
  <div className="raj-center">
    <h4>{text}</h4>
  </div>
);

export default () => {
  let renderRoute = routes.map((detail, key) =>
    detail.isPrivate ? (
      <PrivateRoute key={key} {...detail} />
    ) : (
      <Route key={key} {...detail} />
    )
  );

  return (
    <Suspense fallback={<CenterText text="Loading ..." />}>
      <Switch>
        {renderRoute}
        <Route render={() => <CenterText text=" 404 | Page not found" />} />
      </Switch>
    </Suspense>
  );
};
