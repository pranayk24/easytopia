import React from "react";

import { NavLink } from "react-router-dom";

import classes from "./common.module.scss";

import PATH from "../../../routes/PATH";

const UserHeader = () => {
  return (
    <div className={`${classes.userHeaderWrap} d-flex d-wrap`}>
      <NavLink
        activeClassName={classes.active}
        className={classes.link}
        to={PATH.userSettings}
      >
        Settings
      </NavLink>
      <NavLink
        activeClassName={classes.active}
        className={classes.link}
        to={PATH.userPlan}
      >
        Plan
      </NavLink>
      <NavLink
        activeClassName={classes.active}
        className={classes.link}
        to={PATH.userBilling}
      >
        Billing
      </NavLink>
      <NavLink
        activeClassName={classes.active}
        className={classes.link}
        to={PATH.userInvoice}
      >
        Invoices
      </NavLink>
    </div>
  );
};

export default UserHeader;
