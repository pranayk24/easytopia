import React from "react";

import DiscoverImg from "../../assets/images/discover.png";

import { MDBContainer, MDBBtn, MDBModal, MDBModalBody } from "mdbreact";

import classes from "./index.module.scss";

const DiscoverModal = ({ close }) => {
  return (
    <MDBContainer>
      <MDBModal size="lg" isOpen={true} toggle={close}>
        <MDBModalBody>
          <div className="pt-4 px-5 w-100 d-flex d-flex-wrap flex-column text-center">
            <h1 className={classes.title}>Discover all sharing options</h1>
            <p className={classes.para}>
              He’re is how our powerful Augmented Reality link work on different
              channels
            </p>
            <img className="w-100 m-auto" style={{ maxWidth: 600 }} alt="" src={DiscoverImg} />
            <MDBBtn onClick={close} className="blueBtn mt-5 mx-auto py-2">Close</MDBBtn>
          </div>
        </MDBModalBody>
      </MDBModal>
    </MDBContainer>
  );
};

export default DiscoverModal;
