import { combineReducers } from 'redux';

import authReducer from './auth';
import rootReducer from './root';

export default combineReducers({
  auth: authReducer,
  root: rootReducer
});