import React from "react";

import dashBoardLayout from "../../../hoc/dashBoardLayout";

import classes from "./common.module.scss";

import UserHeader from "./UserHeader";

const UserLayout = ({ children }) => {
  return (
    <div className={classes.mainWrap}>
      <div className="d-flex align-items-center mt-2">
        <div className={classes.avatar}>GF</div>
        <div className="ml-2">
          <div className={classes.name}>Gauthier F.</div>
          <span className={classes.tag}>Business</span>
        </div>
      </div>

      <div className="et-paper mt-4 pt-0">
        <UserHeader />

        <div className="mt-3">{children}</div>
      </div>
    </div>
  );
};

export default dashBoardLayout(UserLayout);
