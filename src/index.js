import React from "react";
import ReactDOM from "react-dom";
import "./index.scss";
import App from "./App";
import * as serviceWorker from "./serviceWorker";

// Design library's
import "@fortawesome/fontawesome-free/css/all.min.css";
import "bootstrap-css-only/css/bootstrap.min.css";
import "mdbreact/dist/css/mdb.css";

// Redux
import { Provider } from "react-redux";
import store from "./redux";

// Toast
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.min.css";




ReactDOM.render(
  <Provider store={store}>
    <App />
    
    <ToastContainer />
  </Provider>,
  document.getElementById("root")
);

serviceWorker.unregister();
