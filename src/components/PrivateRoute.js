import React from "react";

import { Route, Redirect } from "react-router-dom";

import { connect } from "react-redux";

import PATH from "../routes/PATH";

const PrivateRoute = ({ component: Component, token, ...rest }) => {
  const isLogin =
    token && token !== null && token.trim().length > 0 ? true : false;

  return (
    <Route
      {...rest}
      render={(props) =>
        isLogin ? <Component {...props} /> : <Redirect to={PATH.login} />
      }
    />
  );
};

const mapStateToProps = ({ auth: { token } }) => ({
  token: token,
});

export default connect(mapStateToProps)(PrivateRoute);
