import React from "react";
import classes from "./contact.module.scss";
import { MDBBtn } from "mdbreact";
import { Formik, Field } from "formik";
import * as Yup from "yup";
import InputField from "../../components/form/inputField";

// tranlation
import { FormattedMessage } from "react-intl";

const submitContact = Yup.object().shape({
  emailID: Yup.string()
    .email("Invalid email!")
    .required("The Email field is required!"),
  subject: Yup.string().required("The Subject field is required!"),
  message: Yup.string().required("The Message field is required!"),
});

const Contact = () => {
  return (
    <div className={`row custom-input-wrapper ${classes.homeWrapper}`}>
      <div className="col-sm-12 col-lg-4">
        <h1 className={`font-weight-bold m-0  ${classes.heading}`}>
          <FormattedMessage
            id="pages.contact.index.ContactUs"
            defaultMessage="Contact Us,"
          />
        </h1>
        <h5 className={`font-weight-normal m-0 ${classes.subheading}`}>
          {" "}
          <FormattedMessage
            id="pages.contact.index.help"
            defaultMessage="We're here to help you out."
          />
        </h5>
        <Formik
          initialValues={{
            emailID: "",
            subject: "",
            message: "",
          }}
          validationSchema={submitContact}
        >
          {({
            isValid,
            isSubmitting,
            handleSubmit,
            values,
            touched,
            handleChange,
            handleBlur,
          }) => (
            <form className={`${classes.form}`} onSubmit={handleSubmit}>
              <Field
                margin="normal"
                fullwidth="true"
                type="email"
                name="emailID"
                label={
                  <FormattedMessage
                    id="pages.contact.index.Email"
                    defaultMessage="Email"
                  />
                }
                autoComplete="off"
                // touched={touched}
                component={InputField}
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.emailID}
                variant="outlined"
                // autoFocus
                // className={`${classes.input1}`}
              />
              <Field
                margin="normal"
                fullwidth="true"
                type="text"
                name="subject"
                label={
                  <FormattedMessage
                    id="pages.contact.index.Subject"
                    defaultMessage="Subject"
                  />
                }
                autoComplete="off"
                // touched={touched}
                component={InputField}
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.subject}
                variant="outlined"
                // autoFocus
                // className={`${classes.input1}`}
              />
              <p className={`${classes.forgotText} m-0 mb-2 ml-1`}>
                <FormattedMessage
                  id="pages.contact.index.Message"
                  defaultMessage="Message"
                />
              </p>

              <Field
                margin="normal"
                fullwidth="true"
                type="textarea"
                name="message"
                autoComplete="off"
                label={
                  <FormattedMessage
                    id="pages.contact.index.YourMessage"
                    defaultMessage="Your Message"
                  />
                }
                // touched={touched}
                component={InputField}
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.message}
                variant="outlined"
                // autoFocus
                className={`${classes.input3}`}
              />

              <div className="row">
                <div className="col-sm-12 col-md-6 d-md-flex align-items-end"></div>
                <div className="col-sm-12 col-md-6 text-lg-right">
                  <MDBBtn
                    className={`${classes.loginBtn}`}
                    disabled={isSubmitting || !isValid}
                    type="submit"
                  >
                    <FormattedMessage
                      id="pages.contact.index.Send"
                      defaultMessage="Augmented Send,"
                    />
                  </MDBBtn>
                </div>
              </div>
            </form>
          )}
        </Formik>
      </div>
    </div>
  );
};

export default Contact;
