import React, { useState } from "react";

// css
import classes from "./home.module.scss";

// images
import iphoneFrame from "../../assets/images/iphone-frame.svg";
import FileImg from "../../assets/images/file.svg";
import SocialImg from "../../assets/images/social-media.svg";
import EnjoyImg from "../../assets/images/enjoy.svg";
import AnalyzeImg from "../../assets/images/analyze.svg";
import AndroidImg from "../../assets/images/android.svg";
import IosImg from "../../assets/images/ios.svg";

// video
import IphoneVid from "../../assets/main/src_assets_main.mp4";

// Dropzone
import StyledDropzone from "../../components/StyledDropzone";

import { MDBBtn } from "mdbreact";
import TextField from "@material-ui/core/TextField";

// redux
import { connect } from "react-redux";
import * as actions from "../../redux/actions/auth";

// Amazon cognito
import UserPoll from "../../aws/UserPool";
import { CognitoUser, AuthenticationDetails } from "amazon-cognito-identity-js";

//import axios
import axios from "axios";
// import baseurl
import { BASE_URL } from "../../constants/index";

import { Formik, Field } from "formik";
import * as Yup from "yup";
import InputField from "../../components/form/inputField";

import { toastError } from "../../utils";
import { NavLink } from "react-router-dom";
import PATH from "../../routes/PATH";

// import greeting part
import Greeting from "../../components/experinceCreated/index";

// localization
import { FormattedMessage } from "react-intl";

// react vido
// import { Player } from "video-react";
import ReactPlayer from "react-player";

const Home = ({ token, loading, start, end, logIn, cleanUp }) => {
  const [uploadedImage, setUploadedImage] = useState();
  const [isLinkgen, setisLinkgen] = useState(false);
  const [aliasVal, setaliasVal] = useState("https://abc.com/");
  const [isValidForBtn, setValidForBtn] = useState(true);
  const isTokenAvailable =
    token && token !== null && token.trim().length > 0 ? true : false;

  const uploadFileFormValidation = Yup.object().shape({
    email: !isTokenAvailable
      ? Yup.string()
          .trim()
          .email("Invalid email!")
          .required("The Email field is required!")
      : Yup.string(),
    password: !isTokenAvailable
      ? Yup.string().required("The Password field is required!")
      : Yup.string(),

    productLink: Yup.string()
      .trim()
      .matches(
        /(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&=]*)/,
        "URL format is not valid"
      ),
  });

  function getGreeting(data) {
    setisLinkgen(true);
  }
  const changeAlias = (e) => {
    console.log(e.target.value);
    if (e.target.value == "https://abc.com") {
      setaliasVal("https://abc.com/");
    } else {
      setaliasVal(e.target.value);
    }
  };

  const uploadedImageViaAPI = (data, setSubmitting) => {
    console.log(`data =====>`, data);
    let formData = new FormData();
    formData.append("mode", "formdata");
    formData.append("nodes[0].files.glb", uploadedImage);
    formData.append("name", uploadedImage.name);
    formData.append("type", "object"); //
    // formData.append("value", uploadedImage)
    // formData.append("type", "text")

    // var options = { content: formData };

    // axios.post(`${BASE_URL}/3ds/library`, options,function( error, response ) {
    //   if ( error ) {console.log( error );}else{console.log(response)}});
    // console.log("data ==> 1",formData);
    // bodyFormData.append( "nodes[0].files.glßb",uploadedImage)

    // axios({
    //   method: 'post',
    //   url: `${BASE_URL}/3ds/library`,
    //   data: formData,
    //   headers: {
    //         Authorization: `Bearer ${token}`,
    //         "content-type" : "multipart/form-data"
    //       },
    // });

    /*** * main code  */ axios
      .post(`${BASE_URL}/3ds/library`, formData, {
        headers: {
          Authorization: `Bearer ${token}`,
          "content-type": "multipart/form-data",
        },
      })
      .then(function (response) {
        console.log(response, response.data.id);
        let formDataExp = new FormData();
        formDataExp.append("title", "Experince");
        formDataExp.append("description", uploadedImage.name);
        formDataExp.append("triggers.type", "inSpace");
        formDataExp.append("scene.id", response.data.id);
        // formDataExp.append("productLink","object")
        // formDataExp.append("slug","object")
        // formDataExp.append("tags","object")
        formDataExp.append("picturesMainFile", uploadedImage);
        axios
          .post(`${BASE_URL}/3ds/experiences`, formDataExp, {
            headers: {
              Authorization: `Bearer ${token}`,
              "content-type": "multipart/form-data",
            },
          })
          .then(function (response) {
            console.log("res ==>", response);
            getGreeting(true);
          })
          .catch(function (error) {
            console.log("err ==>", error);
          });
      })
      .catch(function (error) {
        console.log("responses", error);
      });

    setSubmitting(false);
    end();
  };

  const uploadImageAuthCheck = async (data, { setSubmitting }) => {
    console.log("image check ", data, uploadedImage);
    if (isTokenAvailable) {
      uploadedImageViaAPI(data, setSubmitting);
    } else {
      start();
      const user = new CognitoUser({
        Username: data.email,
        Pool: UserPoll,
      });

      const authDetails = new AuthenticationDetails({
        Username: data.email,
        // Password: data.password,
      });

      user.authenticateUser(authDetails, {
        onSuccess: async (resp) => {
          console.log("here is the response", resp);
          let token = resp.accessToken.jwtToken;
          await logIn(token);
          uploadedImageViaAPI(data, setSubmitting);
        },
        onFailure: (err) => {
          console.error(`err =====>`, err);
          toastError(
            err.message
              ? err.message
              : "Something went wrong, Please try again."
          );
          end();
          setSubmitting(false);
        },
        // newPasswordRequired: (userAttributes) => {
        //   let sessionUserAttributes = userAttributes;
        //   delete sessionUserAttributes.email_verified;
        //   user.completeNewPasswordChallenge(
        //     data.password,
        //     sessionUserAttributes,
        //     {
        //       onSuccess: async (resp) => {
        //         let token = resp.accessToken.jwtToken;
        //         await logIn(token);
        //         uploadedImage(data, setSubmitting);
        //       },
        //       onFailure: (err) => {
        //         console.error(`err =====>`, err);
        //         toastError(
        //           err.message
        //             ? err.message
        //             : "Something went wrong, Please try again."
        //         );
        //         end();
        //         setSubmitting(false);
        //       },
        //     }
        //   );
        // },
      });
    }
  };
  // function handleBlurx(x) {
  //   console.log(
  //     "whats in product",
  //     /(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&=]*)/.test(
  //       x
  //     )
  //      Yup.object().shape({
  //       productLink: Yup.string()
  //       .trim()
  //       .matches(
  //         /(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&=]*)/,
  //         "URL format is not valid"
  //       ),
  //     }
  //   );
  // }

  return (
    <>
      <video
        autoPlay="autoplay"
        muted="muted"
        className="position-absolute videoStyle"
        playsInline
        // id="videoStyle"
        style={{
          pointerEvents: "none",
          background: " 0% 0% / cover transparent",
          width: "340px",
          right: "150px",
          bottom: "-30px",
          paddingBottom: "194.67px",
          height: "915px",
          objectFit: "cover",
          objectPosition: "center center",
          borderRadius: "35px",
          // borderBottomLeftRadius: "210px",
        }}
      >
        <source src={IphoneVid} type="video/mp4" />
      </video>
      <img
        src={iphoneFrame}
        style={{
          position: "absolute",

          right: "135px",
          bottom: "140px",
          height: "750px",
        }}
      />

      <div className={classes.homeWrapper}>
        <div className="row position-relative h-100 w-100">
          <div className="col col-md-6">
            <div className="row">
              <div className="col-12">
                <h1 className={`font-weight-normal m-0 ${classes.heading}`}>
                  <FormattedMessage
                    id="pages.home.index.Augmented"
                    defaultMessage="Augmented reality,"
                  />
                </h1>
                <h1 className={`font-weight-bold m-0 ${classes.heading}`}>
                  <FormattedMessage
                    id="pages.home.index.madeEasy"
                    defaultMessage="made easy…"
                  />
                </h1>
              </div>

              <div className="col-8 mt-4 d-flex justify-content-between align-items-center">
                <div className="d-flex align-items-center">
                  <img src={FileImg} alt="" />
                  <span className={`mx-2`}>
                    <FormattedMessage
                      id="pages.home.index.Download"
                      defaultMessage="Download"
                    />
                  </span>
                </div>
                <i className="et-arrow et-right"></i>
                <div className="d-flex align-items-center">
                  <img src={SocialImg} alt="" />
                  <span className={`mx-2`}>
                    <FormattedMessage
                      id="pages.home.index.Share"
                      defaultMessage="Share"
                    />
                  </span>
                </div>
                <i className="et-arrow et-right"></i>
                <div className="d-flex align-items-center">
                  <img src={EnjoyImg} alt="" />
                  <span className={`mx-2`}>
                    <FormattedMessage
                      id="pages.home.index.Enjoy"
                      defaultMessage="Enjoy"
                    />
                  </span>
                </div>
                <i className="et-arrow et-right"></i>
                <div className="d-flex align-items-center">
                  <img src={AnalyzeImg} alt="" width="22" height="22" />
                  <span className={`mx-2`}>
                    <FormattedMessage
                      id="pages.home.index.Analyze"
                      defaultMessage="Analyze"
                    />
                  </span>
                </div>
              </div>
              {isLinkgen == false ? (
                <>
                  <div className="col-7 mt-4">
                    <StyledDropzone
                      uploadedImage={uploadedImage}
                      updateUploadedImage={(file) => setUploadedImage(file)}
                    />
                  </div>

                  <div className="col-7 my-2 text-center font-weight-bold">
                    <NavLink className="text-decoreation-none" to={PATH.faq}>
                      <FormattedMessage
                        id="pages.home.index.obtainGLB"
                        defaultMessage="How to obtain GLB ?"
                      />
                    </NavLink>
                  </div>

                  <div className="col-7 mt-4 custom-input-wrapper">
                    <Formik
                      initialValues={{
                        email: "",
                        // alias: "",
                        productLink: "",
                      }}
                      validationSchema={uploadFileFormValidation}
                      onSubmit={uploadImageAuthCheck}
                      // onSubmit={createExperince}
                    >
                      {({
                        isValid,
                        isSubmitting,
                        handleSubmit,
                        values,
                        touched,
                        handleChange,
                        handleBlur,
                      }) => (
                        <form
                          method="POST"
                          className="w-100 mt-2"
                          onSubmit={handleSubmit}
                        >
                          {!isTokenAvailable && (
                            <Field
                              autoComplete="off"
                              className="m-0"
                              type="email"
                              name="email"
                              label="Email"
                              touched={touched}
                              component={InputField}
                              onChange={handleChange}
                              onBlur={handleBlur}
                              value={values.email}
                              variant="outlined"
                            />
                          )}

                          {/* <Field
                        autoComplete="off"
                        className="m-0"
                        type="text"
                        name="alias"
                        label="Alias"
                        touched={touched}
                        component={InputField}
                        onChange={handleChange}
                        // onChange = {changeAlias}
                        onBlur={handleBlur}
                        // value={aliasVal}
                        value={values.alias}
                        variant="outlined"
                      />                          */}
                          <div
                            className="md-form"
                            className="m-0"
                            style={{
                              background: "rgb(245, 245, 245)",
                              borderRadius: "5px",
                              display: "flex",
                              padding: "10px",
                              marginBottom: "10px !important",
                              height: "70px",
                              display: "flex",
                              flexDirection: "column",
                              width: "300.3px",
                            }}
                            // component={InputField}
                          >
                            <label style={{ margin: "0 !important" }}>
                              https://prod.easytopia.app/experiences/
                            </label>

                            <input
                              id="standard-basic"
                              placeholder="link-name"
                              style={{
                                border: "0",
                                height: "30px",
                                backgroundColor: "inherit",
                                width: "100%",
                                left: "0",
                                top: "20px",
                                borderBottom: "1px solid #ddd",
                                borderRadius: "0 !important",
                                // lineHeight: "0px",
                                lineHeight: "normal!important",
                              }}
                            />
                          </div>
                          {console.log("value =>", values.productLink)}
                          <Field
                            autoComplete="off"
                            // className="m-0"
                            type="text"
                            name="productLink"
                            label={
                              <FormattedMessage
                                id="component.Dashboard.Experince.ProductLink"
                                defaultMessage="Product Link"
                              />
                            }
                            touched={touched}
                            component={InputField}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.productLink}
                            variant="outlined"
                            style={{ marginTop: "30px" }}
                          />

                          <MDBBtn
                            disabled={isSubmitting || !isValid || loading}
                            className="blueBtn w-100 m-0"
                            type="submit"
                          >
                            <FormattedMessage
                              id="pages.home.index.GetLink"
                              defaultMessage="Get a Link"
                            />
                          </MDBBtn>
                        </form>
                      )}
                    </Formik>
                  </div>
                </>
              ) : (
                <Greeting />
              )}
              <div className="col-7 mt-5 mb-3 d-flex justify-content-between">
                <a
                  target="_blank"
                  rel="noopener noreferrer"
                  href="https://play.google.com/store/apps/details?id=fr.easytopia.app"
                >
                  <img className="et-pointer" src={AndroidImg} alt="" />
                </a>
                <a
                  target="_blank"
                  rel="noopener noreferrer"
                  href="https://apps.apple.com/us/app/easytopia/id1381399321"
                >
                  <img className="et-pointer" src={IosImg} alt="" />
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

const mapStateToProps = ({ auth: { token, loading } }) => ({
  token: token,
  loading: loading,
});

const mapDispatchToProps = {
  start: actions.startLoader,
  end: actions.endLoader,
  logIn: actions.logIn,
  cleanUp: actions.clean,
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
