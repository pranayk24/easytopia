import { toast } from "react-toastify";

import { toastConfig } from "../constants";

export function formatBytes(a, b = 2) {
  if (0 === a) return "0 Bytes";
  const c = 0 > b ? 0 : b,
    d = Math.floor(Math.log(a) / Math.log(1024));
  return (
    parseFloat((a / Math.pow(1024, d)).toFixed(c)) +
    " " +
    ["Bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"][d]
  );
}

export const toastSuccess = (message) => {
  toast.success(message, toastConfig);
};

export const toastInfo = (message) => {
  toast.info(message, toastConfig);
};

export const toastWarning = (message) => {
  toast.warn(message, toastConfig);
};

export const toastError = (message) => {
  toast.error(message, toastConfig);
};

export const toastDefault = (message) => {
  toast(message, toastConfig);
};

const initAuth = () => {
  return window.gapi.auth2.init({
    client_id: "UA-168238020-2", //paste your client ID here
    scope: "https://www.googleapis.com/auth/analytics.readonly",
  });
};
export const checkSignedIn = () => {
  return new Promise((resolve, reject) => {
    initAuth() //calls the previous function
      .then(() => {
        const auth = window.gapi.auth2.getAuthInstance(); //returns the GoogleAuth object
        resolve(auth.isSignedIn.get()); //returns whether the current user is currently signed in
      })
      .catch((error) => {
        reject(error);
      });
  });
};
export const renderButton = () => {
  window.gapi.signin2.render("signin-button", {
    scope: "profile email",
    width: 240,
    height: 50,
    longtitle: true,
    theme: "dark",
    onsuccess: onSuccess,
    onfailure: onFailure,
  });
};

const onSuccess = (googleUser) => {
  console.log("Logged in as: " + googleUser.getBasicProfile().getName());
};

const onFailure = (error) => {
  console.error(error);
};
