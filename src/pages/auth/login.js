import React, { useEffect } from "react";

import classes from "./login.module.scss";

import { MDBBtn } from "mdbreact";

// Amazon cognito
import UserPoll from "../../aws/UserPool";
import { CognitoUser, AuthenticationDetails } from "amazon-cognito-identity-js";

// redux
import { connect } from "react-redux";
import * as actions from "../../redux/actions/auth";

//
import { Formik, Field } from "formik";
import * as Yup from "yup";
import InputField from "../../components/form/inputField";
import { Redirect, NavLink } from "react-router-dom";
import { toastError } from "../../utils";

import PATH from "../../routes/PATH";

const loginFormValidation = Yup.object().shape({
  email: Yup.string().required("The Email field is required!"),
  password: Yup.string().required("The Password field is required!"),
});

const Login = ({ auth, start, end, logIn, cleanUp }) => {
  const { token, loading } = auth;

  useEffect(() => {
    return () => {
      cleanUp();
    };
  }, [cleanUp]);

  if (token && typeof token === "string" && token.trim().length > 0) {
    return <Redirect to={PATH.main} />;
  }

  return (
    <div className={`row custom-input-wrapper ${classes.loginWrapper}`}>
      {console.log("lc", loginFormValidation)}
      <Formik
        initialValues={{
          email: "",
          password: "",
        }}
        validationSchema={loginFormValidation}
        onSubmit={async ({ email, password }, { setSubmitting }) => {
          start();
          const user = new CognitoUser({
            Username: email,
            Pool: UserPoll,
          });

          const authDetails = new AuthenticationDetails({
            Username: email,
            Password: password,
          });

          user.authenticateUser(authDetails, {
            onSuccess: async (data) => {
              console.log("data=>! ", data);
              // let token = data.accessToken.jwtToken;
              let token = data.idToken.jwtToken;
              await logIn(token);
              setSubmitting(false);
              end();
            },
            onFailure: (err) => {
              console.error(`err =====>`, err);
              toastError(
                err.message
                  ? err.message
                  : "Something went wrong, Please try again."
              );
              end();
              setSubmitting(false);
            },
            newPasswordRequired: (userAttributes) => {
              let sessionUserAttributes = userAttributes;
              delete sessionUserAttributes.email_verified;
              user.completeNewPasswordChallenge(
                password,
                sessionUserAttributes,
                {
                  onSuccess: async (data) => {
                    console.log("data=>! ", data);
                    // let token = data.accessToken.jwtToken;
                    let token = data.idToken.jwtToken;
                    await logIn(token);
                    setSubmitting(false);
                    end();
                  },
                  onFailure: (err) => {
                    console.error(`err =====>`, err);
                    toastError(
                      err.message
                        ? err.message
                        : "Something went wrong, Please try again."
                    );
                    end();
                    setSubmitting(false);
                  },
                }
              );
            },
          });
        }}
      >
        {({
          isValid,
          isSubmitting,
          handleSubmit,
          values,
          touched,
          handleChange,
          handleBlur,
        }) => (
          <form method="POST" className="w-100" onSubmit={handleSubmit}>
            <div className="col-sm-12 col-lg-4">
              <h1 className={`mt-5 ${classes.headerText}`}>Login</h1>
              <h5 className={classes.subHeaderText}>Welcome back!</h5>

              <Field
                autoComplete="off"
                className="m-0 mt-4"
                name="email"
                label="Email or Username"
                touched={touched}
                component={InputField}
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.email}
                variant="outlined"
              />

              <Field
                className="m-0"
                type="password"
                name="password"
                label="Password"
                touched={touched}
                component={InputField}
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.password}
                variant="outlined"
              />

              <div className="row">
                <div className="col-sm-12 col-md-6 d-md-flex align-items-center"></div>
                <div className="col-sm-12 text-lg-right">
                  <MDBBtn
                    disabled={isSubmitting || !isValid || loading}
                    className={`blueBtn w-100 m-0 mb-4`}
                    type="submit"
                  >
                    Login
                  </MDBBtn>

                  <div className={classes.forgotText}>
                    Forgot your password?
                  </div>

                  <div className={`${classes.forgotText} mt-2`}>
                    Don't have an account?{" "}
                    <NavLink
                      className={`${classes.textBlue}  text-decoration-none`}
                      to={PATH.register}
                    >
                      Register
                    </NavLink>
                    .
                  </div>
                </div>
              </div>
            </div>
          </form>
        )}
      </Formik>
    </div>
  );
};

const mapStateToProps = ({ auth }) => ({
  auth: auth,
});

const mapDispatchToProps = {
  start: actions.startLoader,
  end: actions.endLoader,
  logIn: actions.logIn,
  cleanUp: actions.clean,
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
