import React from "react";

import { MDBCol, MDBContainer, MDBRow, MDBFooter } from "mdbreact";

import { NavLink } from "react-router-dom";

import Logo from "../../assets/images/logo.png";
import PATH from "../../routes/PATH";

import LegalNoticePDF from "../../assets/files/legal-notice.pdf";
import PrivacyPDF from "../../assets/files/privacy.pdf";
import ServiceAgreementPDF from "../../assets/files/service-agreement.pdf";

// tranlation
import { FormattedMessage } from "react-intl";

const Footer = () => {
  return (
    <div
      id="et-footer"
      className="pt-4 mt-4"
      style={{ position: "sticky", zIndex: "101" }}
    >
      <MDBFooter className="font-small w-100 pt-4">
        <MDBContainer className="text-center text-md-left">
          <MDBRow>
            <MDBCol md="2">
              <img alt="" src={Logo} width={80} />
            </MDBCol>
            <MDBCol md="3">
              <h5 className="title font-weight-normal">
                <FormattedMessage
                  id="footer.PLATFORM"
                  defaultMessage="PLATFORM"
                />
              </h5>
              <ul className="p-0">
                <li className="list-unstyled">
                  <NavLink to="/features">
                    <FormattedMessage
                      id="footer.Features"
                      defaultMessage="Features"
                    />
                  </NavLink>
                </li>
                <li className="list-unstyled">
                  <NavLink to={PATH.pricing}>
                    <FormattedMessage
                      id="footer.Pricing"
                      defaultMessage="Pricing"
                    />
                  </NavLink>
                </li>
              </ul>
            </MDBCol>
            <MDBCol md="3">
              <h5 className="title font-weight-normal">
                <FormattedMessage
                  id="footer.COMPANY"
                  defaultMessage="COMPANY"
                />
              </h5>
              <ul className="p-0">
                <li className="list-unstyled">
                  <NavLink to={PATH.about}>
                    About
                    <FormattedMessage
                      id="footer.About"
                      defaultMessage="About"
                    />
                  </NavLink>
                </li>
                <li className="list-unstyled">
                  <a
                    rel="noopener noreferrer"
                    target="_blank"
                    href={LegalNoticePDF}
                  >
                    <FormattedMessage
                      id="footer.LegalNotice"
                      defaultMessage="Legal Notice"
                    />
                  </a>
                </li>
                <li className="list-unstyled">
                  <a
                    rel="noopener noreferrer"
                    target="_blank"
                    href={PrivacyPDF}
                  >
                    <FormattedMessage
                      id="footer.PrivacyPolicy"
                      defaultMessage="Privacy Policy"
                    />
                  </a>
                </li>
                <li className="list-unstyled">
                  <a
                    rel="noopener noreferrer"
                    target="_blank"
                    href={ServiceAgreementPDF}
                  >
                    <FormattedMessage
                      id="footer.ServiceAgreement"
                      defaultMessage="Service Agreement"
                    />
                  </a>
                </li>
              </ul>
            </MDBCol>
            <MDBCol md="3">
              <h5 className="title font-weight-normal">
                <FormattedMessage
                  id="Header.contact"
                  defaultMessage="CONTACT"
                />
              </h5>
              <ul className="p-0">
                <li className="list-unstyled">
                  <a href="mailto:Contact@easytopia.fr">Contact@easytopia.fr</a>
                </li>
                <li className="list-unstyled">
                  <a href="#!">+33(6)37110648</a>
                </li>
                <li className="list-unstyled mt-3">
                  <a
                    href="https://twitter.com/easytopia"
                    rel="noopener noreferrer"
                    target="_blank"
                    className="tw-ic"
                  >
                    <i className="fab fa-twitter fa-lg white-text mr-3 fa-1.5x"></i>
                  </a>
                  <a
                    href="https://www.linkedin.com/company/easytopia"
                    rel="noopener noreferrer"
                    target="_blank"
                    className="li-ic"
                  >
                    <i className="fab fa-linkedin-in fa-lg white-text mr-3 fa-1.5x"></i>
                  </a>
                  <a
                    href="https://www.instagram.com/easytopia"
                    rel="noopener noreferrer"
                    target="_blank"
                    className="ins-ic"
                  >
                    <i className="fab fa-instagram fa-lg white-text mr-3 fa-1.5x"></i>
                  </a>
                </li>
              </ul>
            </MDBCol>
          </MDBRow>
        </MDBContainer>
        <div className="footer-copyright text-right py-2 ">
          <MDBContainer fluid>
            &copy; {new Date().getFullYear()} Copyright:
            <a
              rel="noopener noreferrer"
              target="_blank"
              href="https://www.easytopia.fr"
            >
              Easytopia
            </a>
          </MDBContainer>
        </div>
      </MDBFooter>
    </div>
  );
};

export default Footer;
