import React,{useState} from 'react';
// import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
// import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Link from '@material-ui/core/Link';
import TextField from '@material-ui/core/TextField';
import BitmapImg from '../../assets/images/Bitmap2.png';
import { MDBBtn } from "mdbreact";

// import toaster
import { toastSuccess } from "../../utils/index";

  // import Greeting from '../experinceCreated/index'
export default function Done() {
//   const classes = useStyles();
const [getLink, setGetLink] = useState(['https://easytopia.fr/8364HBR']);

  return (
    
//     <div>
// <Greeting />

//     </div>
    <Grid
    container
    spacing={0}
    // direction="column"
    alignItems="center"
    // justify="center"
   
    style={{  justifyContent: "end" , top:"100%" }}
   >
  
 
    <Card elevation={0} style={{width : "350px", marginTop: "56px" }}  >
    
    <CardActionArea style={{justifyContent: "center",pointerEvents:"none",
    display: "flex",
    flexDirection: "column"}} >
      
        <CardMedia
          component="img"
          alt="Contemplative Reptile"
          height="100"
          src = {BitmapImg}
          title="Contemplative Reptile"
          style={{width:"150px", height:"100px" }}
        />
        
        <CardContent>
        {/* <ButtonBase disabled> */}
          <Typography gutterBottom  component="h2"  style={{textAlign:"center" , fontSize:"initial" , fontWeight:"bold"}}>
            You're done!
          </Typography>
          {/* </ButtonBase> */}
          <Typography gutterBottom variant="h8"   style={{textAlign:"center", color:"#CBCECF"}}>
            Link valid for 7 days.
          </Typography>
          </CardContent>
      </CardActionArea> 
          <Typography gutterBottom variant="h8"   style={{textAlign:"center" , fontWeight:"bold" , fontSize:"initial"}} disabled={false}>
          <Link href="#" >
            Get unlimited link validity
          </Link>
          </Typography>
          
       
          
            { getLink.map((item, index) =>( 
              <>
      <CardActions>
      <TextField id="filled-basic" value={item} style={{backgroundColor:"#EDF0F0" , width:"100%" , borderColor:"white"  , borderWidth:"0px"}} readOnly variant="outlined" />
      
      </CardActions>
           
      <MDBBtn  className="blueBtn w-100 m-0"   onClick={(_) => {
                            navigator.clipboard.writeText(item);
                            toastSuccess(`${item} copied successfully.`);}}>
                        Copy Link
                      </MDBBtn>
                      </>
     ))}
    </Card>
    

 </Grid>
  
  
  );
}
