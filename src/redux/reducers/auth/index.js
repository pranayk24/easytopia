import {
  AUTH_START,
  AUTH_END,
  AUTH_FAIL,
  AUTH_SUCCESS,
  CLEAN_UP,
  SIGN_OUT,
  LANG,
} from "../../constants/auth";
import { set } from "date-fns";

const initState = {
  token: localStorage.getItem("token") ? localStorage.getItem("token") : null,
  error: null,
  loading: false,
  language: localStorage.getItem("crispLang")
    ? localStorage.getItem("crispLang")
    : "en",
  changExp: null,
};

const authReducer = (state = initState, { type, payload }) => {
  console.log("asdfgh", payload);
  switch (type) {
    case AUTH_START:
      return {
        ...state,
        loading: true,
      };

    case AUTH_END:
      return {
        ...state,
        loading: false,
      };

    case AUTH_FAIL:
      return {
        ...state,
        error: payload,
      };

    case AUTH_SUCCESS:
      return {
        ...state,
        token: payload.token,
        error: false,
      };

    case SIGN_OUT:
      return {
        ...state,
        token: null,
        error: false,
      };

    case CLEAN_UP:
      return {
        ...state,
        error: null,
        loading: false,
      };

    case LANG:
      return {
        ...state,
        error: null,
        loading: false,
        language: payload,
      };
    case "CHANG_EXPERIENCE":
      return {
        ...state,
        error: null,
        loading: false,
        changExp: payload,
      };

    default:
      return state;
  }
};

export default authReducer;
