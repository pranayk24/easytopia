import React, { useState, useEffect, useRef } from "react";

import dashBoardLayout from "../../../hoc/dashBoardLayout";

import classes from "./analytics.module.scss";
import { MDBBtn, MDBIcon, MDBDataTableV5 } from "mdbreact";

import { addDays } from "date-fns";
import { DateRangePicker } from "react-date-range";
import "react-date-range/dist/styles.css"; // main style file
import "react-date-range/dist/theme/default.css"; // theme css file

import { renderButton, checkSignedIn } from "../../../utils/index";

import { toastError } from "../../../utils";

import Report from "./report";
import axios from "axios";
import { connect } from "react-redux";
import { BASE_URL } from "../../../constants";

// import CanvasJS from "canvasjs";
import Chartjs from "chart.js";
import { Bar, Doughnut, Chart } from "react-chartjs-2";

const Analytics = (token) => {
  const [state, setState] = useState([
    {
      startDate: new Date(),
      endDate: addDays(new Date(), 7),
      key: "selection",
      color: "#1bc9e6",
    },
  ]);

  const [showCal, setShowCal] = useState(false);
  const [analyticData, setAnalyticData] = useState([]);
  const [styleBtn, setStyleBtn] = useState("day");

  // New Aanalytics code ....*******
  const [isSignedIn, setIsSignedIn] = useState(false);

  const updateSignin = (signedIn) => {
    //(3)
    setIsSignedIn(signedIn);
    if (!signedIn) {
      renderButton();
    }
  };
  const init = () => {
    //(2)
    checkSignedIn()
      .then((signedIn) => {
        updateSignin(signedIn);
      })
      .catch((error) => {
        console.error(error);
      });
  };
  useEffect(() => {
    window.gapi.load("auth2", init); //(1)
  });

  const data = {
    columns: [
      {
        label: "NAME",
        field: "name",
        sort: false,
      },
      {
        label: "DATE",
        field: "date",
        sort: false,
      },
      {
        label: "IN-LINK VIEWS",
        field: "in",
        sort: false,
      },
      {
        label: "OUT-LINK VIEWS",
        field: "out",
        sort: false,
      },
      {
        label: "Average Time",
        field: "avgTime",
        sort: false,
      },
    ],

    rows: [
      ...analyticData.map((item, index) => {
        console.log(item, index);
        return {
          name: <div className={classes.name}>Raj Shah</div>,
          date: (
            <div className={classes.field}>
              <MDBIcon className="mr-2" icon="calendar" />
              <span>4/05/2020</span>
            </div>
          ),
          in: (
            <div className={classes.field}>
              <MDBIcon className="mr-2" icon="eye" />
              <span>{item.inLink}</span>
            </div>
          ),
          out: (
            <div className={classes.field}>
              <MDBIcon className="mr-2" icon="eye" />

              <span>{item.outLink}</span>
            </div>
          ),
          avgTime: (
            <div className={classes.field}>
              <MDBIcon className="mr-2" />
              <span>{item.avgTime}</span>
            </div>
          ),
        };
      }),
    ],
  };

  const postAnalytics = (dates) => {
    console.log(dates, dates.startDate, dates.endDate);
    let today = dates.startDate;
    let today1 = dates.endDate;
    var month = today.getMonth() + 1;
    var newmonth = month < 10 ? "0" + month : month; // if month is number
    var date = today.getDate();
    var newDate = date < 10 ? "0" + date : date; // if month is number

    var month1 = today1.getMonth() + 1;
    var newmonth1 = month1 < 10 ? "0" + month1 : month1; // if month is number
    var date1 = today1.getDate();
    var newDate1 = date1 < 10 ? "0" + date1 : date1; // if month is number
    console.log("=>", newmonth1);
    let dateStart = today.getFullYear() + "-" + newmonth + "-" + newDate;
    console.log("final", dateStart);

    let dateEnd = today1.getFullYear() + "-" + newmonth1 + "-" + newDate1;

    axios
      .post(
        `${BASE_URL}/analytics`,
        { startDate: dateStart, endDate: dateEnd, groupBy: styleBtn },
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      )
      .then((response) => {
        console.log("Response", response.data);
        // setAnalyticData(response.data);
        var temp = response.data;
        var newAnalyticsObj = [];
        console.log("length =>", response.data.length);
        temp.map(function (item, i) {
          // console.log("is ", item, i);
          item.referrers.map(function (item1, j) {
            if (j == 0) {
              item1.data.map(function (item2, k) {
                item2.events.map(function (item3, l) {
                  if (item3.eventAction == "click-ar-button") {
                    var x = parseFloat(item2.avgTimeOnPage);
                    var avgTimePage = x.toFixed(2);

                    var newData = {
                      ID: item.experienceId,
                      inLink: item2.pageViews,
                      outLink: 0,
                      avgTime: avgTimePage,
                      linkFormat: "In-links",
                      totalView: item2.pageViews,
                      uniqueView: item2.uniquePageViews,
                    };
                    newAnalyticsObj.push(newData);
                  } else if (item3.eventAction == "click-out-link") {
                    var x = parseFloat(item2.avgTimeOnPage);
                    var avgTimePage = x.toFixed(2);
                    var newData = {
                      ID: item.experienceId,
                      inLink: item2.pageViews,
                      outLink: item3.totalEvents,
                      avgTime: avgTimePage,
                      linkFormat: "Out-links",
                      totalView: item2.pageViews,
                      uniqueView: item2.uniquePageViews,
                    };
                    newAnalyticsObj.push(newData);
                  }
                });
              });
            } else {
              console.log("there are more refernce");
            }
          });
        });
        setAnalyticData(newAnalyticsObj);
        // setData(response.data.data);
      })
      .catch((err) => {
        console.log("Error", err);
        toastError(
          err.message ? err.message : "Something went wrong, Please try again."
        );
      });
  };

  const clickButton = (val) => {
    console.log("clciked", val);
    axios
      .post(
        `${BASE_URL}/analytics`,
        { groupBy: val },
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      )
      .then((response) => {
        console.log("Response", response.data);
        // setAnalyticData(response.data);
        var temp = response.data;
        var newAnalyticsObj = [];
        console.log("length =>", response.data.length);
        temp.map(function (item, i) {
          // console.log("is ", item, i);
          item.referrers.map(function (item1, j) {
            if (j == 0) {
              item1.data.map(function (item2, k) {
                item2.events.map(function (item3, l) {
                  if (item3.eventAction == "click-ar-button") {
                    var x = parseFloat(item2.avgTimeOnPage);
                    var avgTimePage = x.toFixed(2);

                    var newData = {
                      ID: item.experienceId,
                      inLink: item2.pageViews,
                      outLink: 0,
                      avgTime: avgTimePage,
                      linkFormat: "In-links",
                      totalView: item2.pageViews,
                      uniqueView: item2.uniquePageViews,
                    };
                    newAnalyticsObj.push(newData);
                  } else if (item3.eventAction == "click-out-link") {
                    var x = parseFloat(item2.avgTimeOnPage);
                    var avgTimePage = x.toFixed(2);
                    var newData = {
                      ID: item.experienceId,
                      inLink: item2.pageViews,
                      outLink: item3.totalEvents,
                      avgTime: avgTimePage,
                      linkFormat: "Out-links",
                      totalView: item2.pageViews,
                      uniqueView: item2.uniquePageViews,
                    };
                    newAnalyticsObj.push(newData);
                  }
                });
              });
            } else {
              console.log("there are more refernce");
            }
          });
        });
        setAnalyticData(newAnalyticsObj);
        // setData(response.data.data);
      })
      .catch((err) => {
        console.log("Error", err);
        toastError(
          err.message ? err.message : "Something went wrong, Please try again."
        );
      });
  };

  useEffect(() => {
    clickButton(styleBtn);
  }, [styleBtn]);

  useEffect(() => {
    chart(analyticData);
    chartout(analyticData);
    chart1(analyticData);
    chart1out(analyticData);
  }, [analyticData]);
  // chart
  const [chartData, setChartData] = useState({});

  var labelUniqueViewInlink = [];
  var labelTotalViewInlink = [];
  var lengthInlink = [];
  var indexCountInlink = 0;
  var totalViewIn = 0;
  var totalUniqueIn = 0;
  // var labelUniqueViewOutlink = [];
  // var labelTotalViewOutlink = [];
  const chart = (x) => {
    x.map((item, index) => {
      if (item.linkFormat == "In-links") {
        totalUniqueIn = totalUniqueIn + parseInt(item.uniqueView);
        totalViewIn = totalViewIn + parseInt(item.totalView);
        labelUniqueViewInlink.push(item.uniqueView);
        labelTotalViewInlink.push(item.totalView);
        lengthInlink.push(indexCountInlink.toString());
        indexCountInlink = indexCountInlink + 1;
      } else {
        // labelUniqueViewOutlink.push(item.uniqueView);
        // labelTotalViewOutlink.push(item.totalView);
      }
      console.log("Data=>1", lengthInlink);
    });
    if (labelUniqueViewInlink == 0) {
      setChartData(false);
    } else {
      setChartData({
        labels: lengthInlink,
        backgroundColor: ["rgba(75,192,192,0.6)"],
        datasets: [
          {
            label: "Unique views",
            backgroundColor: "#1bc9e6",
            data: labelUniqueViewInlink,
          },
          {
            label: "Total views",
            backgroundColor: "#A4E6F1",
            data: labelTotalViewInlink,
          },
        ],
        text: ["23%"],
      });
    }
  };
  // chart
  const [chartDataOut, setChartDataOut] = useState({});

  // var labelUniqueViewInlink = [];
  // var labelTotalViewInlink = [];
  // var lengthInlink = [];
  var labelUniqueViewOutlink = [];
  var labelTotalViewOutlink = [];
  var lengthOutlink = [];
  var indexCount = 0;
  var totalUnique = 0;
  var totalView = 0;

  const chartout = (x) => {
    x.map((item, index) => {
      console.log("outLink", item.linkFormat, item);
      if (item.linkFormat == "In-links") {
        // labelUniqueViewInlink.push(item.uniqueView);
        // labelTotalViewInlink.push(item.totalView);
        // lengthInlink.push(index.toString());
      } else {
        totalUnique = totalUnique + parseInt(item.uniqueView);
        totalView = totalView + parseInt(item.totalView);
        labelUniqueViewOutlink.push(item.uniqueView);
        labelTotalViewOutlink.push(item.totalView);
        lengthOutlink.push(indexCount.toString());
        indexCount = indexCount + 1;
      }
      console.log(
        "Data=>",
        labelUniqueViewOutlink,
        labelTotalViewOutlink,
        lengthOutlink
      );
    });
    console.log("labelUniqueViewOutlink", labelUniqueViewOutlink.length);
    if (labelUniqueViewOutlink == 0) {
      setChartDataOut(false);
    } else {
      setChartDataOut({
        labels: lengthOutlink,
        backgroundColor: ["rgba(75,192,192,0.6)"],
        datasets: [
          {
            label: "Unique views",
            backgroundColor: "#1bc9e6",
            data: labelUniqueViewOutlink,
          },
          {
            label: "Total views",
            backgroundColor: "#A4E6F1",
            data: labelTotalViewOutlink,
          },
        ],
      });
    }
  };

  //  pulgin text center doughnut
  var originalDoughnutDraw = Chart.controllers.doughnut.prototype.draw;
  Chart.helpers.extend(Chart.controllers.doughnut.prototype, {
    draw: function () {
      originalDoughnutDraw.apply(this, arguments);

      var chart = this.chart;
      var width = chart.chart.width,
        height = chart.chart.height,
        ctx = chart.chart.ctx;

      var fontSize = (height / 114).toFixed(2);
      ctx.font = fontSize + "em sans-serif";
      ctx.textBaseline = "middle";

      var sum = 0;
      for (var i = 0; i < chart.config.data.datasets[0].data.length; i++) {
        console.log("==>avg", chart.config.data.datasets[0].data[i]);
        sum =
          (chart.config.data.datasets[0].data[1] /
            (chart.config.data.datasets[0].data[0] +
              chart.config.data.datasets[0].data[1])) *
          100;
      }

      var text = sum.toFixed(0),
        textX = Math.round((width - ctx.measureText(text).width) / 2.1),
        textY = height / 1.5;
      if (text == "NaN") {
        text = "";
        ctx.fillText(text, textX, textY);
      } else {
        ctx.fillText(text + "%", textX, textY);
      }
    },
  });
  const [chartDataDougnut, setchartDataDougnut] = useState({});
  // useEffect(() => {

  // }, [styleBtn]);
  const chart1 = () => {
    // var dognutData = [];

    var TV = parseInt(totalViewIn);
    var TU = parseInt(totalUniqueIn);
    console.log("whats in dognutData", TV, TU);
    setchartDataDougnut({
      labels: ["Total views", "Unique views"],
      backgroundColor: ["rgba(75,192,192,0.6)"],
      datasets: [
        {
          backgroundColor: ["#A4E6F1", "#1bc9e6"],
          data: [TV, TU],
        },
      ],
      text: "23%",
    });
  };

  const [chartDataDougnutOut, setchartDataDougnutOut] = useState({});
  // useEffect(() => {

  // }, [styleBtn]);
  const chart1out = () => {
    // var dognutData = [];

    var TV = parseInt(totalView);
    var TU = parseInt(totalUnique);
    console.log("whats in dognutData", TV, TU);
    setchartDataDougnutOut({
      labels: ["Total views", "Unique views"],
      backgroundColor: ["rgba(75,192,192,0.6)"],
      datasets: [
        {
          backgroundColor: ["#A4E6F1", "#1bc9e6"],
          data: [TV, TU],
        },
      ],
    });
  };

  return (
    <div className={classes.mainWrapper}>
      <div className="row">
        <div className="col-sm-12 col-md-6 text-md-left">
          <h3 className={classes.heading}>Analytics</h3>
          <h4 className={classes.subHeading}>Updated just now</h4>
        </div>
        <div className="col-sm-12 col-md-6 text-md-right">
          <MDBBtn
            onClick={(_) => setShowCal(!showCal)}
            className="blueBtn px-3"
          >
            <span>All time</span>
            <MDBIcon className="ml-3" icon="angle-down" />
          </MDBBtn>
        </div>
        {showCal && (
          <div className="animated fadeIn ml-auto" style={{ paddingRight: 15 }}>
            <DateRangePicker
              onChange={(item) => {
                console.log(`item ====>`, item, item.selection.startDate);
                setState([item.selection]);
                postAnalytics(item.selection);
              }}
              colo
              showPreview={false}
              showSelectionPreview={true}
              moveRangeOnFirstSelection={false}
              months={1}
              ranges={state}
            />
          </div>
        )}
      </div>

      <div className="et-paper mt-4">
        <div>
          {!isSignedIn ? <div id="signin-button"></div> : <Report />}
          <div className={classes.threeButton}>
            <div className={classes.paperHeading}>Campaign overview</div>
            <div>
              <button
                className={
                  styleBtn === "day"
                    ? classes.buttonsEnable
                    : classes.buttonsDisable
                }
                onClick={() => {
                  setStyleBtn("day");
                  // clickButton("day");
                }}
              >
                By day
              </button>
              <button
                className={
                  styleBtn === "week"
                    ? classes.buttonsEnable
                    : classes.buttonsDisable
                }
                onClick={() => {
                  setStyleBtn("week");
                  // clickButton("week");
                }}
              >
                By week{" "}
              </button>
              <button
                className={
                  styleBtn === "month"
                    ? classes.buttonsEnable
                    : classes.buttonsDisable
                }
                onClick={() => {
                  setStyleBtn("month");
                  // clickButton("month");
                }}
              >
                {" "}
                By month
              </button>
            </div>
          </div>
          <div style={{ display: "flex", flexDirection: "row" }}>
            {console.log("datachart", chartData)}
            {chartData ? (
              <div
                style={{ width: "25%", display: "flex", flexDirection: "row" }}
              >
                <Bar
                  data={chartData}
                  width={30}
                  height={25}
                  options={{
                    animation: {
                      animateScale: false,
                    },
                    legend: {
                      display: false,
                    },
                    tooltips: {
                      callbacks: {
                        label: function (tooltipItem) {
                          return tooltipItem.yLabel;
                        },
                      },
                    },
                    responsive: true,
                    title: {
                      text: "In-links",
                      position: "top",
                      fontSize: "15",

                      display: true,
                    },
                    scales: {
                      yAxes: [
                        {
                          display: false,
                          ticks: {
                            autoSkip: true,
                            maxTicksLimit: 10,
                            beginAtZero: true,
                          },
                          gridLines: {
                            display: false,
                          },
                        },
                      ],
                      xAxes: [
                        {
                          barPercentage: 0.4,
                          gridLines: {
                            display: false,
                          },
                        },
                      ],
                    },
                  }}
                />
                <Doughnut
                  data={chartDataDougnut}
                  width={20}
                  height={15}
                  options={{
                    responsive: true,
                    cutoutPercentage: 75,
                    rotation: -2.02 * Math.PI,
                    // centertext: "23%",
                    scales: {
                      yAxes: [
                        {
                          display: false,
                          ticks: {
                            autoSkip: true,
                            maxTicksLimit: 10,
                            beginAtZero: true,
                          },
                          gridLines: {
                            display: false,
                          },
                        },
                      ],
                      xAxes: [
                        {
                          display: false,
                          barPercentage: 0.4,
                          gridLines: {
                            display: false,
                          },
                        },
                      ],
                    },
                  }}
                />
              </div>
            ) : (
              <div className={classes.conditionalDivIn}>
                {" "}
                No data available for In-links
              </div>
            )}

            {chartData ? <div className={classes.vertical}></div> : <div />}

            {console.log("datachart", chartDataOut)}
            {chartDataOut ? (
              <div
                style={{
                  width: "25%",
                  display: "flex",
                  flexDirection: "row",
                  marginLeft: "22%",
                }}
              >
                <Bar
                  data={chartDataOut}
                  width={30}
                  height={25}
                  options={{
                    legend: {
                      display: false,
                    },
                    tooltips: {
                      callbacks: {
                        label: function (tooltipItem) {
                          return tooltipItem.yLabel;
                        },
                      },
                    },
                    responsive: true,
                    title: {
                      text: "Out-links",

                      display: true,
                    },
                    scales: {
                      yAxes: [
                        {
                          display: false,
                          ticks: {
                            autoSkip: true,
                            maxTicksLimit: 10,
                            beginAtZero: true,
                          },
                          gridLines: {
                            display: false,
                          },
                        },
                      ],
                      xAxes: [
                        {
                          barPercentage: 0.4,
                          gridLines: {
                            display: false,
                          },
                        },
                      ],
                    },
                  }}
                />
                <Doughnut
                  data={chartDataDougnutOut}
                  width={20}
                  height={15}
                  options={{
                    title: {
                      text: "AR Engagement rate",
                      position: "top",
                      fontSize: "15",
                      // padding: "0.5",
                      display: true,
                    },
                    legend: {
                      display: false,
                    },
                    responsive: true,
                    cutoutPercentage: 75,
                    rotation: -2.02 * Math.PI,
                    scales: {
                      yAxes: [
                        {
                          display: false,
                          ticks: {
                            autoSkip: true,
                            maxTicksLimit: 10,
                            beginAtZero: true,
                          },
                          gridLines: {
                            display: false,
                          },
                        },
                      ],
                      xAxes: [
                        {
                          display: false,
                          barPercentage: 0.4,
                          gridLines: {
                            display: false,
                          },
                        },
                      ],
                    },
                  }}
                />
              </div>
            ) : (
              <div className={classes.conditionalDiv}>
                {" "}
                No data available for Out-links
              </div>
            )}
          </div>
        </div>
        <hr />
        <div>
          <div className={classes.paperHeading}>Experiences</div>
          <MDBDataTableV5
            entries={5}
            noBottomColumns
            searching={false}
            sortable={false}
            bordered={false}
            responsive
            small
            displayEntries={false}
            data={data}
          />
        </div>
      </div>
    </div>
  );
};

// export default dashBoardLayout(Analytics);
const mapStateToProps = ({ auth: { token } }) => ({
  token: token,
});
// export default Pricing;
export default connect(mapStateToProps)(dashBoardLayout(Analytics));
