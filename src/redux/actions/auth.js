import {
  AUTH_START,
  AUTH_END,
  AUTH_SUCCESS,
  CLEAN_UP,
  SIGN_OUT,
  LANG,
} from "../constants/auth";

import axios from "axios";

import { BASE_URL } from "../../constants";
import { ME_GET_SUCCESS, ME_GET_FAIL, ME_CLEAR } from "../constants/root";
import { toastError } from "../../utils";

export const logOut = (_) => async (dispatch) => {
  try {
    localStorage.clear();
    dispatch({
      type: ME_CLEAR,
    });
    dispatch({
      type: SIGN_OUT,
    });
  } catch (error) {
    console.error(error);
  }
};

// Login action creator
export const logIn = (token) => async (dispatch) => {
  axios
    .get(`${BASE_URL}/me`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
    .then((resp) => {
      localStorage.setItem("user", JSON.stringify(resp.data));
      localStorage.setItem("token", token);
      dispatch({
        type: AUTH_SUCCESS,
        payload: { token: token },
      });
      dispatch({
        type: ME_GET_SUCCESS,
        payload: resp.data,
      });
    })
    .catch((err) => {
      toastError("Something went wrong! Please try again after sometime,");
      dispatch({
        type: ME_GET_FAIL,
        payload: err.message,
      });
    });
};

export const startLoader = (_) => async (dispatch) => {
  dispatch({
    type: AUTH_START,
  });
};

export const endLoader = (_) => async (dispatch) => {
  dispatch({
    type: AUTH_END,
  });
};

// cleanup messages action creator
export const clean = (_) => async (dispatch) => {
  dispatch({
    type: CLEAN_UP,
  });
};

//  tranlator
export const langChange = (data) => async (dispatch) => {
  dispatch({
    type: LANG,
    payload: data,
  });
};
export const changExperience = (data) => async (dispatch) => {
  dispatch({
    type: "CHANG_EXPERIENCE",
    payload: data,
  });
};
