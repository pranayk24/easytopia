import React, { useMemo, useEffect, useState } from "react";

// Dropdown
import { useDropzone } from "react-dropzone";

// CSS
import classes from "./styled_dropzone.module.scss";

// router
import { NavLink } from "react-router-dom";

// utils
import { formatBytes } from "../../utils";

// Images
import BitmapImage from "../../assets/images/bitmap.png";
import GlbImage from "../../assets/images/glb_file.svg";
import TrashBinImage from "../../assets/images/delete_bin.svg";

import PATH from "../../routes/PATH";
// locatization import
import { FormattedMessage } from "react-intl";

const baseStyle = {
  cursor: "pointer",
  flex: 1,
  display: "flex",
  flexDirection: "column",
  alignItems: "center",
  padding: "6px 0 0 0",
  borderWidth: 1,
  borderRadius: 20,
  borderColor: "#8C8C8C",
  borderStyle: "dashed",
  backgroundColor: "#fff",
  color: "#000",
  outline: "none",
  transition: "border .24s ease-in-out",
};

const activeStyle = {
  borderColor: "#2196f3",
};

const acceptStyle = {
  borderColor: "#2196f3",
};

const rejectStyle = {
  borderColor: "#2196f3",
};

const StyledDropzone = ({ uploadedImage, updateUploadedImage }) => {
  const {
    acceptedFiles,
    getRootProps,
    getInputProps,
    isDragActive,
    isDragAccept,
    isDragReject,
  } = useDropzone({ accept: ".glb, .gltf, .usdz", multiple: false });
  const [files, setFiles] = useState(acceptedFiles);

  useEffect(() => {
    if (files[0] !== uploadedImage) {
      updateUploadedImage(files[0]);
    }
  }, [files, uploadedImage, updateUploadedImage]);

  const acceptedFilesItems = files.map((file) => (
    <div key={file.path} style={{ maxWidth: 170 }}>
      <img className="mb-2 pt-2" src={GlbImage} alt="" />

      <div className={classes.initialText}>{file.path}</div>
      <div className={`mt-1 ${classes.initialTextInfo}`}>
        {formatBytes(file.size)}
      </div>

      <img
        onClick={(e) => {
          e.preventDefault();
          e.stopPropagation();
          acceptedFiles.length = 0;
          acceptedFiles.splice(0, acceptedFiles.length);
          updateUploadedImage(undefined);
          setFiles([]);
        }}
        className="et-pointer mt-3 pb-2"
        src={TrashBinImage}
        alt=""
      />
    </div>
  ));

  const style = useMemo(
    () => ({
      ...baseStyle,
      ...(isDragActive ? activeStyle : {}),
      ...(isDragAccept ? acceptStyle : {}),
      ...(isDragReject ? rejectStyle : {}),
    }),
    [isDragActive, isDragReject, isDragAccept]
  );

  useEffect(() => {
    setFiles(acceptedFiles);
  }, [acceptedFiles]);

  return (
    <div className="">
      <div {...getRootProps({ style })}>
        {acceptedFilesItems && acceptedFilesItems.length < 1 && (
          <div className="text-center">
            <input {...getInputProps()} />
            <img className="mb-2" src={BitmapImage} alt="" />
            <p className={classes.initialText} style={{ maxWidth: 170 }}>
              <span>
                <FormattedMessage
                  id="component.StyleedDrop.DropyourGLBfile"
                  defaultMessage="Drop your GLB file here or"
                />
              </span>
              <span className="ml-1" style={{ color: "#0070FF" }}>
                <FormattedMessage
                  id="component.StyleedDrop.browse"
                  defaultMessage="Browse"
                />
              </span>
            </p>
            <p className={`mt-1 ${classes.initialTextInfo}`}>
              <span>
                <FormattedMessage
                  id="component.StyleedDrop.Max"
                  defaultMessage="Max 50MB and 7 days"
                />
              </span>
              <NavLink
                onClick={(e) => e.stopPropagation()}
                to={PATH.pricing}
                style={{ color: "#0070FF" }}
                className="ml-1"
              >
                <FormattedMessage
                  id="component.StyleedDrop.Getmore"
                  defaultMessage="Get more"
                />
              </NavLink>
            </p>
          </div>
        )}
        <div className="text-center" style={{ maxWidth: 170 }}>
          {acceptedFilesItems}
        </div>
      </div>
    </div>
  );
};

export default StyledDropzone;
