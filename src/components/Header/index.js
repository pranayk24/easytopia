import React, { useState } from "react";

// routing
import { NavLink } from "react-router-dom";

// images
import Logo from "../../assets/images/logo.png";
import EnFlag from "../../assets/images/united-states-of-america.png";
import FrFlag from "../../assets/images/france.png";

// scss
import classes from "./header.module.scss";

// redux
import { connect } from "react-redux";

import PATH from "../../routes/PATH";

import { FormattedMessage } from "react-intl";

import { langChange } from "../../redux/actions/auth";
import { lang } from "moment";

import { Helmet } from "react-helmet";

// ReactDOMServer.renderToString(<Handler />);
// const helmet = Helmet.renderStatic();

const Header = ({ token, langChange, language }) => {
  const [showLanguageDropDown, setShowLanguageDropDown] = useState(false);
  window.language = language;
  const isTokenAvailable =
    token && token !== null && token.trim().length > 0 ? true : false;

  const changeCrisp = (lang) => {
    // window.lang = lang;
  };

  return (
    <nav
      className={`container-fluid flex-wrap header-style-one d-flex align-items-center text-decoration-none ${classes.headerWrapper}`}
    >
      {/* <Helmet>
        <script type="text/javascript">
          {`window.intialVal = window.language;`}
        </script>
        <script type="text/javascript">
          {`
            window.CRISP_RUNTIME_CONFIG = {
              locale: window.language,
            }
          `}
        </script>
        <script type="text/javascript">
          {`
          window.$crisp=[];
          window.CRISP_WEBSITE_ID="15a6d903-10b3-4fdc-86da-4a0c8a8eed68";
          
          d=document;
          s=d.createElement("script");
          s.src="https://client.crisp.chat/l.js";
          s.async=1;
          d.getElementsByTagName("head")[0].appendChild(s);
          `}
        </script>
      </Helmet> */}
      <NavLink
        to={PATH.main}
        className="navbar-brand d-flex align-items-center"
      >
        <img
          className="d-inline-block align-top"
          width="34"
          height="34"
          src={Logo}
          alt=""
        />
        <b className={`${classes.headerLogoText} font-weight-bold ml-1`}>
          EASYTOPIA
        </b>
      </NavLink>
      <NavLink
        className={`ml-auto font-weight-medium text-decoration-none ${classes.headerText}`}
        to={PATH.pricing}
      >
        <FormattedMessage id="Header.price" defaultMessage="Pricing" />
      </NavLink>
      <NavLink
        className={`font-weight-medium ml-4 text-decoration-none ${classes.headerText}`}
        to={PATH.contact}
      >
        <FormattedMessage id="Header.contact" defaultMessage="Contact" />
      </NavLink>
      {isTokenAvailable && (
        <NavLink
          className={`font-weight-medium ml-4 text-decoration-none ${classes.headerText}`}
          to={PATH.dashboard}
        >
          <FormattedMessage id="Header.dashboard" defaultMessage="Dashboard" />
        </NavLink>
      )}
      {isTokenAvailable && (
        <NavLink
          className={`font-weight-medium ml-4 text-decoration-none ${classes.headerText} ${classes.loginBtn}`}
          to={PATH.logout}
        >
          <FormattedMessage id="Header.Logout" defaultMessage="Logout" />
        </NavLink>
      )}
      {!isTokenAvailable && (
        <NavLink
          className={`font-weight-medium ml-4 text-decoration-none ${classes.headerText} ${classes.loginBtn}`}
          to={PATH.login}
        >
          <FormattedMessage id="Header.Login" defaultMessage="Login" />
        </NavLink>
      )}

      <div className={classes.customLanguageSelection}>
        <label
          onClick={(_) => setShowLanguageDropDown(!showLanguageDropDown)}
          className="m-0 ml-3"
          htmlFor="languageSelection"
        >
          <img width={32} src={language === "en" ? EnFlag : FrFlag} alt="" />
        </label>
        {showLanguageDropDown && (
          <ul className="animated fadeIn">
            <li
              onClick={() => {
                localStorage.setItem("crispLang", "en");
                window.intialVal = "en";
                changeCrisp("en");
                langChange("en");
                setShowLanguageDropDown(!showLanguageDropDown);
              }}
            >
              <img width={24} src={EnFlag} alt="" />
              English
            </li>
            <li
              onClick={() => {
                localStorage.setItem("crispLang", "fr");
                window.intialVal = "fr";
                changeCrisp("fr");
                langChange("fr");
                setShowLanguageDropDown(!showLanguageDropDown);
              }}
            >
              <img width={24} src={FrFlag} alt="" />
              French
            </li>
          </ul>
        )}
      </div>
    </nav>
  );
};

const mapStateToProps = ({ auth: { token, language, flag } }) => ({
  token: token,
  language: language,
  flag: flag,
});
const mapDispatchToProps = {
  langChange,
};
export default connect(mapStateToProps, mapDispatchToProps)(Header);
