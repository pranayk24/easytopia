import React, { useEffect } from "react";

// Application routes
import Routes from "./routes";

// routing
import { BrowserRouter } from "react-router-dom";

// component
import Header from "./components/Header/index";
import Footer from "./components/Footer";

import { Elements } from "@stripe/react-stripe-js";
import { loadStripe } from "@stripe/stripe-js";

import { IntlProvider } from "react-intl";
import English from "./language/en.json";
import French from "./language/fr.json";
// redux
import { connect } from "react-redux";

import ReactGa from "react-ga";

const stripePromise = loadStripe(process.env.REACT_APP_STRIPE_PK);

const App = (props) => {
  // console.log("whats lang", props.language);
  // if (props.language == "fr") {
  //   window.$crisp.push(["do", "chat:hide"]);
  // }

  useEffect(() => {
    ReactGa.initialize("UA-168238020-2");
    // Report page view

    ReactGa.pageview(window.location.pathname + window.location.search);
  });

  window.$crisp = [];
  window.CRISP_WEBSITE_ID = "15a6d903-10b3-4fdc-86da-4a0c8a8eed68";
  (function () {
    window.CRISP_RUNTIME_CONFIG = {
      locale: props.language,
    };
    var d = document;
    var s = d.createElement("script");
    s.src = "https://client.crisp.chat/l.js";
    s.async = 1;
    d.getElementsByTagName("head")[0].appendChild(s);
  })();

  const messages = {
    en: English,
    fr: French,
  };
  return (
    <IntlProvider locale={props.language} messages={messages[props.language]}>
      <BrowserRouter>
        <Header />
        <Elements stripe={stripePromise}>
          <div
            className="container et-container"
            style={{ marginBottom: "0 auto -200px" }}
          >
            <Routes />
          </div>
        </Elements>
        <Footer />
      </BrowserRouter>
    </IntlProvider>
  );
};

// export default App;
const mapStateToProps = ({ auth: { token, language } }) => ({
  token: token,
  language: language,
});

export default connect(mapStateToProps)(App);
